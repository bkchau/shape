package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.ShapeRequirement;
import com.shape.domain.Shape;
import com.shape.domain.Requirement;
import com.shape.repository.ShapeRequirementRepository;
import com.shape.service.ShapeRequirementService;
import com.shape.service.dto.ShapeRequirementDTO;
import com.shape.service.mapper.ShapeRequirementMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ShapeRequirementResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class ShapeRequirementResourceIT {

    private static final Float DEFAULT_VALUE = 1F;
    private static final Float UPDATED_VALUE = 2F;

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    @Autowired
    private ShapeRequirementRepository shapeRequirementRepository;

    @Autowired
    private ShapeRequirementMapper shapeRequirementMapper;

    @Autowired
    private ShapeRequirementService shapeRequirementService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShapeRequirementMockMvc;

    private ShapeRequirement shapeRequirement;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShapeRequirementResource shapeRequirementResource = new ShapeRequirementResource(shapeRequirementService);
        this.restShapeRequirementMockMvc = MockMvcBuilders.standaloneSetup(shapeRequirementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShapeRequirement createEntity(EntityManager em) {
        ShapeRequirement shapeRequirement = new ShapeRequirement()
            .value(DEFAULT_VALUE)
            .unit(DEFAULT_UNIT);
        // Add required entity
        Shape shape;
        if (TestUtil.findAll(em, Shape.class).isEmpty()) {
            shape = ShapeResourceIT.createEntity(em);
            em.persist(shape);
            em.flush();
        } else {
            shape = TestUtil.findAll(em, Shape.class).get(0);
        }
        shapeRequirement.setShape(shape);
        // Add required entity
        Requirement requirement;
        if (TestUtil.findAll(em, Requirement.class).isEmpty()) {
            requirement = RequirementResourceIT.createEntity(em);
            em.persist(requirement);
            em.flush();
        } else {
            requirement = TestUtil.findAll(em, Requirement.class).get(0);
        }
        shapeRequirement.setRequirement(requirement);
        return shapeRequirement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShapeRequirement createUpdatedEntity(EntityManager em) {
        ShapeRequirement shapeRequirement = new ShapeRequirement()
            .value(UPDATED_VALUE)
            .unit(UPDATED_UNIT);
        // Add required entity
        Shape shape;
        if (TestUtil.findAll(em, Shape.class).isEmpty()) {
            shape = ShapeResourceIT.createUpdatedEntity(em);
            em.persist(shape);
            em.flush();
        } else {
            shape = TestUtil.findAll(em, Shape.class).get(0);
        }
        shapeRequirement.setShape(shape);
        // Add required entity
        Requirement requirement;
        if (TestUtil.findAll(em, Requirement.class).isEmpty()) {
            requirement = RequirementResourceIT.createUpdatedEntity(em);
            em.persist(requirement);
            em.flush();
        } else {
            requirement = TestUtil.findAll(em, Requirement.class).get(0);
        }
        shapeRequirement.setRequirement(requirement);
        return shapeRequirement;
    }

    @BeforeEach
    public void initTest() {
        shapeRequirement = createEntity(em);
    }

    @Test
    @Transactional
    public void createShapeRequirement() throws Exception {
        int databaseSizeBeforeCreate = shapeRequirementRepository.findAll().size();

        // Create the ShapeRequirement
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(shapeRequirement);
        restShapeRequirementMockMvc.perform(post("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isCreated());

        // Validate the ShapeRequirement in the database
        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeCreate + 1);
        ShapeRequirement testShapeRequirement = shapeRequirementList.get(shapeRequirementList.size() - 1);
        assertThat(testShapeRequirement.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testShapeRequirement.getUnit()).isEqualTo(DEFAULT_UNIT);
    }

    @Test
    @Transactional
    public void createShapeRequirementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shapeRequirementRepository.findAll().size();

        // Create the ShapeRequirement with an existing ID
        shapeRequirement.setId(1L);
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(shapeRequirement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShapeRequirementMockMvc.perform(post("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShapeRequirement in the database
        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = shapeRequirementRepository.findAll().size();
        // set the field null
        shapeRequirement.setValue(null);

        // Create the ShapeRequirement, which fails.
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(shapeRequirement);

        restShapeRequirementMockMvc.perform(post("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isBadRequest());

        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnitIsRequired() throws Exception {
        int databaseSizeBeforeTest = shapeRequirementRepository.findAll().size();
        // set the field null
        shapeRequirement.setUnit(null);

        // Create the ShapeRequirement, which fails.
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(shapeRequirement);

        restShapeRequirementMockMvc.perform(post("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isBadRequest());

        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShapeRequirements() throws Exception {
        // Initialize the database
        shapeRequirementRepository.saveAndFlush(shapeRequirement);

        // Get all the shapeRequirementList
        restShapeRequirementMockMvc.perform(get("/api/shape-requirements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shapeRequirement.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())));
    }
    
    @Test
    @Transactional
    public void getShapeRequirement() throws Exception {
        // Initialize the database
        shapeRequirementRepository.saveAndFlush(shapeRequirement);

        // Get the shapeRequirement
        restShapeRequirementMockMvc.perform(get("/api/shape-requirements/{id}", shapeRequirement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shapeRequirement.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.doubleValue()))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingShapeRequirement() throws Exception {
        // Get the shapeRequirement
        restShapeRequirementMockMvc.perform(get("/api/shape-requirements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShapeRequirement() throws Exception {
        // Initialize the database
        shapeRequirementRepository.saveAndFlush(shapeRequirement);

        int databaseSizeBeforeUpdate = shapeRequirementRepository.findAll().size();

        // Update the shapeRequirement
        ShapeRequirement updatedShapeRequirement = shapeRequirementRepository.findById(shapeRequirement.getId()).get();
        // Disconnect from session so that the updates on updatedShapeRequirement are not directly saved in db
        em.detach(updatedShapeRequirement);
        updatedShapeRequirement
            .value(UPDATED_VALUE)
            .unit(UPDATED_UNIT);
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(updatedShapeRequirement);

        restShapeRequirementMockMvc.perform(put("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isOk());

        // Validate the ShapeRequirement in the database
        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeUpdate);
        ShapeRequirement testShapeRequirement = shapeRequirementList.get(shapeRequirementList.size() - 1);
        assertThat(testShapeRequirement.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testShapeRequirement.getUnit()).isEqualTo(UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void updateNonExistingShapeRequirement() throws Exception {
        int databaseSizeBeforeUpdate = shapeRequirementRepository.findAll().size();

        // Create the ShapeRequirement
        ShapeRequirementDTO shapeRequirementDTO = shapeRequirementMapper.toDto(shapeRequirement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShapeRequirementMockMvc.perform(put("/api/shape-requirements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeRequirementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShapeRequirement in the database
        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShapeRequirement() throws Exception {
        // Initialize the database
        shapeRequirementRepository.saveAndFlush(shapeRequirement);

        int databaseSizeBeforeDelete = shapeRequirementRepository.findAll().size();

        // Delete the shapeRequirement
        restShapeRequirementMockMvc.perform(delete("/api/shape-requirements/{id}", shapeRequirement.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShapeRequirement> shapeRequirementList = shapeRequirementRepository.findAll();
        assertThat(shapeRequirementList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShapeRequirement.class);
        ShapeRequirement shapeRequirement1 = new ShapeRequirement();
        shapeRequirement1.setId(1L);
        ShapeRequirement shapeRequirement2 = new ShapeRequirement();
        shapeRequirement2.setId(shapeRequirement1.getId());
        assertThat(shapeRequirement1).isEqualTo(shapeRequirement2);
        shapeRequirement2.setId(2L);
        assertThat(shapeRequirement1).isNotEqualTo(shapeRequirement2);
        shapeRequirement1.setId(null);
        assertThat(shapeRequirement1).isNotEqualTo(shapeRequirement2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShapeRequirementDTO.class);
        ShapeRequirementDTO shapeRequirementDTO1 = new ShapeRequirementDTO();
        shapeRequirementDTO1.setId(1L);
        ShapeRequirementDTO shapeRequirementDTO2 = new ShapeRequirementDTO();
        assertThat(shapeRequirementDTO1).isNotEqualTo(shapeRequirementDTO2);
        shapeRequirementDTO2.setId(shapeRequirementDTO1.getId());
        assertThat(shapeRequirementDTO1).isEqualTo(shapeRequirementDTO2);
        shapeRequirementDTO2.setId(2L);
        assertThat(shapeRequirementDTO1).isNotEqualTo(shapeRequirementDTO2);
        shapeRequirementDTO1.setId(null);
        assertThat(shapeRequirementDTO1).isNotEqualTo(shapeRequirementDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shapeRequirementMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shapeRequirementMapper.fromId(null)).isNull();
    }
}
