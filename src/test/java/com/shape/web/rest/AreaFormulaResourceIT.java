package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.AreaFormula;
import com.shape.domain.RequirementSet;
import com.shape.repository.AreaFormulaRepository;
import com.shape.service.AreaFormulaService;
import com.shape.service.dto.AreaFormulaDTO;
import com.shape.service.mapper.AreaFormulaMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link AreaFormulaResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class AreaFormulaResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_OPERATOR = "AAAAAAAAAA";
    private static final String UPDATED_OPERATOR = "BBBBBBBBBB";

    private static final String DEFAULT_FORMULA_KEY = "AAAAAAAAAA";
    private static final String UPDATED_FORMULA_KEY = "BBBBBBBBBB";

    @Autowired
    private AreaFormulaRepository areaFormulaRepository;

    @Autowired
    private AreaFormulaMapper areaFormulaMapper;

    @Autowired
    private AreaFormulaService areaFormulaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAreaFormulaMockMvc;

    private AreaFormula areaFormula;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AreaFormulaResource areaFormulaResource = new AreaFormulaResource(areaFormulaService);
        this.restAreaFormulaMockMvc = MockMvcBuilders.standaloneSetup(areaFormulaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AreaFormula createEntity(EntityManager em) {
        AreaFormula areaFormula = new AreaFormula()
            .type(DEFAULT_TYPE)
            .operator(DEFAULT_OPERATOR)
            .formulaKey(DEFAULT_FORMULA_KEY);
        // Add required entity
        RequirementSet requirementSet;
        if (TestUtil.findAll(em, RequirementSet.class).isEmpty()) {
            requirementSet = RequirementSetResourceIT.createEntity(em);
            em.persist(requirementSet);
            em.flush();
        } else {
            requirementSet = TestUtil.findAll(em, RequirementSet.class).get(0);
        }
        areaFormula.setRequirementSet(requirementSet);
        return areaFormula;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AreaFormula createUpdatedEntity(EntityManager em) {
        AreaFormula areaFormula = new AreaFormula()
            .type(UPDATED_TYPE)
            .operator(UPDATED_OPERATOR)
            .formulaKey(UPDATED_FORMULA_KEY);
        // Add required entity
        RequirementSet requirementSet;
        if (TestUtil.findAll(em, RequirementSet.class).isEmpty()) {
            requirementSet = RequirementSetResourceIT.createUpdatedEntity(em);
            em.persist(requirementSet);
            em.flush();
        } else {
            requirementSet = TestUtil.findAll(em, RequirementSet.class).get(0);
        }
        areaFormula.setRequirementSet(requirementSet);
        return areaFormula;
    }

    @BeforeEach
    public void initTest() {
        areaFormula = createEntity(em);
    }

    @Test
    @Transactional
    public void createAreaFormula() throws Exception {
        int databaseSizeBeforeCreate = areaFormulaRepository.findAll().size();

        // Create the AreaFormula
        AreaFormulaDTO areaFormulaDTO = areaFormulaMapper.toDto(areaFormula);
        restAreaFormulaMockMvc.perform(post("/api/area-formulas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaFormulaDTO)))
            .andExpect(status().isCreated());

        // Validate the AreaFormula in the database
        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeCreate + 1);
        AreaFormula testAreaFormula = areaFormulaList.get(areaFormulaList.size() - 1);
        assertThat(testAreaFormula.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testAreaFormula.getOperator()).isEqualTo(DEFAULT_OPERATOR);
        assertThat(testAreaFormula.getFormulaKey()).isEqualTo(DEFAULT_FORMULA_KEY);
    }

    @Test
    @Transactional
    public void createAreaFormulaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = areaFormulaRepository.findAll().size();

        // Create the AreaFormula with an existing ID
        areaFormula.setId(1L);
        AreaFormulaDTO areaFormulaDTO = areaFormulaMapper.toDto(areaFormula);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAreaFormulaMockMvc.perform(post("/api/area-formulas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaFormulaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AreaFormula in the database
        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = areaFormulaRepository.findAll().size();
        // set the field null
        areaFormula.setType(null);

        // Create the AreaFormula, which fails.
        AreaFormulaDTO areaFormulaDTO = areaFormulaMapper.toDto(areaFormula);

        restAreaFormulaMockMvc.perform(post("/api/area-formulas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaFormulaDTO)))
            .andExpect(status().isBadRequest());

        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAreaFormulas() throws Exception {
        // Initialize the database
        areaFormulaRepository.saveAndFlush(areaFormula);

        // Get all the areaFormulaList
        restAreaFormulaMockMvc.perform(get("/api/area-formulas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areaFormula.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].operator").value(hasItem(DEFAULT_OPERATOR.toString())))
            .andExpect(jsonPath("$.[*].formulaKey").value(hasItem(DEFAULT_FORMULA_KEY.toString())));
    }
    
    @Test
    @Transactional
    public void getAreaFormula() throws Exception {
        // Initialize the database
        areaFormulaRepository.saveAndFlush(areaFormula);

        // Get the areaFormula
        restAreaFormulaMockMvc.perform(get("/api/area-formulas/{id}", areaFormula.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(areaFormula.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.operator").value(DEFAULT_OPERATOR.toString()))
            .andExpect(jsonPath("$.formulaKey").value(DEFAULT_FORMULA_KEY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAreaFormula() throws Exception {
        // Get the areaFormula
        restAreaFormulaMockMvc.perform(get("/api/area-formulas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAreaFormula() throws Exception {
        // Initialize the database
        areaFormulaRepository.saveAndFlush(areaFormula);

        int databaseSizeBeforeUpdate = areaFormulaRepository.findAll().size();

        // Update the areaFormula
        AreaFormula updatedAreaFormula = areaFormulaRepository.findById(areaFormula.getId()).get();
        // Disconnect from session so that the updates on updatedAreaFormula are not directly saved in db
        em.detach(updatedAreaFormula);
        updatedAreaFormula
            .type(UPDATED_TYPE)
            .operator(UPDATED_OPERATOR)
            .formulaKey(UPDATED_FORMULA_KEY);
        AreaFormulaDTO areaFormulaDTO = areaFormulaMapper.toDto(updatedAreaFormula);

        restAreaFormulaMockMvc.perform(put("/api/area-formulas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaFormulaDTO)))
            .andExpect(status().isOk());

        // Validate the AreaFormula in the database
        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeUpdate);
        AreaFormula testAreaFormula = areaFormulaList.get(areaFormulaList.size() - 1);
        assertThat(testAreaFormula.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testAreaFormula.getOperator()).isEqualTo(UPDATED_OPERATOR);
        assertThat(testAreaFormula.getFormulaKey()).isEqualTo(UPDATED_FORMULA_KEY);
    }

    @Test
    @Transactional
    public void updateNonExistingAreaFormula() throws Exception {
        int databaseSizeBeforeUpdate = areaFormulaRepository.findAll().size();

        // Create the AreaFormula
        AreaFormulaDTO areaFormulaDTO = areaFormulaMapper.toDto(areaFormula);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAreaFormulaMockMvc.perform(put("/api/area-formulas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaFormulaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AreaFormula in the database
        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAreaFormula() throws Exception {
        // Initialize the database
        areaFormulaRepository.saveAndFlush(areaFormula);

        int databaseSizeBeforeDelete = areaFormulaRepository.findAll().size();

        // Delete the areaFormula
        restAreaFormulaMockMvc.perform(delete("/api/area-formulas/{id}", areaFormula.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AreaFormula> areaFormulaList = areaFormulaRepository.findAll();
        assertThat(areaFormulaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AreaFormula.class);
        AreaFormula areaFormula1 = new AreaFormula();
        areaFormula1.setId(1L);
        AreaFormula areaFormula2 = new AreaFormula();
        areaFormula2.setId(areaFormula1.getId());
        assertThat(areaFormula1).isEqualTo(areaFormula2);
        areaFormula2.setId(2L);
        assertThat(areaFormula1).isNotEqualTo(areaFormula2);
        areaFormula1.setId(null);
        assertThat(areaFormula1).isNotEqualTo(areaFormula2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AreaFormulaDTO.class);
        AreaFormulaDTO areaFormulaDTO1 = new AreaFormulaDTO();
        areaFormulaDTO1.setId(1L);
        AreaFormulaDTO areaFormulaDTO2 = new AreaFormulaDTO();
        assertThat(areaFormulaDTO1).isNotEqualTo(areaFormulaDTO2);
        areaFormulaDTO2.setId(areaFormulaDTO1.getId());
        assertThat(areaFormulaDTO1).isEqualTo(areaFormulaDTO2);
        areaFormulaDTO2.setId(2L);
        assertThat(areaFormulaDTO1).isNotEqualTo(areaFormulaDTO2);
        areaFormulaDTO1.setId(null);
        assertThat(areaFormulaDTO1).isNotEqualTo(areaFormulaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(areaFormulaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(areaFormulaMapper.fromId(null)).isNull();
    }
}
