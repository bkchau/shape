package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.RequirementSet;
import com.shape.domain.ShapeCategory;
import com.shape.repository.RequirementSetRepository;
import com.shape.service.RequirementSetService;
import com.shape.service.dto.RequirementSetDTO;
import com.shape.service.mapper.RequirementSetMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link RequirementSetResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class RequirementSetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private RequirementSetRepository requirementSetRepository;

    @Mock
    private RequirementSetRepository requirementSetRepositoryMock;

    @Autowired
    private RequirementSetMapper requirementSetMapper;

    @Mock
    private RequirementSetService requirementSetServiceMock;

    @Autowired
    private RequirementSetService requirementSetService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRequirementSetMockMvc;

    private RequirementSet requirementSet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RequirementSetResource requirementSetResource = new RequirementSetResource(requirementSetService);
        this.restRequirementSetMockMvc = MockMvcBuilders.standaloneSetup(requirementSetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequirementSet createEntity(EntityManager em) {
        RequirementSet requirementSet = new RequirementSet()
            .name(DEFAULT_NAME);
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        requirementSet.setShapeCategory(shapeCategory);
        return requirementSet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequirementSet createUpdatedEntity(EntityManager em) {
        RequirementSet requirementSet = new RequirementSet()
            .name(UPDATED_NAME);
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createUpdatedEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        requirementSet.setShapeCategory(shapeCategory);
        return requirementSet;
    }

    @BeforeEach
    public void initTest() {
        requirementSet = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequirementSet() throws Exception {
        int databaseSizeBeforeCreate = requirementSetRepository.findAll().size();

        // Create the RequirementSet
        RequirementSetDTO requirementSetDTO = requirementSetMapper.toDto(requirementSet);
        restRequirementSetMockMvc.perform(post("/api/requirement-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementSetDTO)))
            .andExpect(status().isCreated());

        // Validate the RequirementSet in the database
        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeCreate + 1);
        RequirementSet testRequirementSet = requirementSetList.get(requirementSetList.size() - 1);
        assertThat(testRequirementSet.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createRequirementSetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requirementSetRepository.findAll().size();

        // Create the RequirementSet with an existing ID
        requirementSet.setId(1L);
        RequirementSetDTO requirementSetDTO = requirementSetMapper.toDto(requirementSet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequirementSetMockMvc.perform(post("/api/requirement-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequirementSet in the database
        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = requirementSetRepository.findAll().size();
        // set the field null
        requirementSet.setName(null);

        // Create the RequirementSet, which fails.
        RequirementSetDTO requirementSetDTO = requirementSetMapper.toDto(requirementSet);

        restRequirementSetMockMvc.perform(post("/api/requirement-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementSetDTO)))
            .andExpect(status().isBadRequest());

        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRequirementSets() throws Exception {
        // Initialize the database
        requirementSetRepository.saveAndFlush(requirementSet);

        // Get all the requirementSetList
        restRequirementSetMockMvc.perform(get("/api/requirement-sets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requirementSet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllRequirementSetsWithEagerRelationshipsIsEnabled() throws Exception {
        RequirementSetResource requirementSetResource = new RequirementSetResource(requirementSetServiceMock);
        when(requirementSetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restRequirementSetMockMvc = MockMvcBuilders.standaloneSetup(requirementSetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restRequirementSetMockMvc.perform(get("/api/requirement-sets?eagerload=true"))
        .andExpect(status().isOk());

        verify(requirementSetServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllRequirementSetsWithEagerRelationshipsIsNotEnabled() throws Exception {
        RequirementSetResource requirementSetResource = new RequirementSetResource(requirementSetServiceMock);
            when(requirementSetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restRequirementSetMockMvc = MockMvcBuilders.standaloneSetup(requirementSetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restRequirementSetMockMvc.perform(get("/api/requirement-sets?eagerload=true"))
        .andExpect(status().isOk());

            verify(requirementSetServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getRequirementSet() throws Exception {
        // Initialize the database
        requirementSetRepository.saveAndFlush(requirementSet);

        // Get the requirementSet
        restRequirementSetMockMvc.perform(get("/api/requirement-sets/{id}", requirementSet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(requirementSet.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRequirementSet() throws Exception {
        // Get the requirementSet
        restRequirementSetMockMvc.perform(get("/api/requirement-sets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequirementSet() throws Exception {
        // Initialize the database
        requirementSetRepository.saveAndFlush(requirementSet);

        int databaseSizeBeforeUpdate = requirementSetRepository.findAll().size();

        // Update the requirementSet
        RequirementSet updatedRequirementSet = requirementSetRepository.findById(requirementSet.getId()).get();
        // Disconnect from session so that the updates on updatedRequirementSet are not directly saved in db
        em.detach(updatedRequirementSet);
        updatedRequirementSet
            .name(UPDATED_NAME);
        RequirementSetDTO requirementSetDTO = requirementSetMapper.toDto(updatedRequirementSet);

        restRequirementSetMockMvc.perform(put("/api/requirement-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementSetDTO)))
            .andExpect(status().isOk());

        // Validate the RequirementSet in the database
        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeUpdate);
        RequirementSet testRequirementSet = requirementSetList.get(requirementSetList.size() - 1);
        assertThat(testRequirementSet.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingRequirementSet() throws Exception {
        int databaseSizeBeforeUpdate = requirementSetRepository.findAll().size();

        // Create the RequirementSet
        RequirementSetDTO requirementSetDTO = requirementSetMapper.toDto(requirementSet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequirementSetMockMvc.perform(put("/api/requirement-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requirementSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequirementSet in the database
        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRequirementSet() throws Exception {
        // Initialize the database
        requirementSetRepository.saveAndFlush(requirementSet);

        int databaseSizeBeforeDelete = requirementSetRepository.findAll().size();

        // Delete the requirementSet
        restRequirementSetMockMvc.perform(delete("/api/requirement-sets/{id}", requirementSet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RequirementSet> requirementSetList = requirementSetRepository.findAll();
        assertThat(requirementSetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequirementSet.class);
        RequirementSet requirementSet1 = new RequirementSet();
        requirementSet1.setId(1L);
        RequirementSet requirementSet2 = new RequirementSet();
        requirementSet2.setId(requirementSet1.getId());
        assertThat(requirementSet1).isEqualTo(requirementSet2);
        requirementSet2.setId(2L);
        assertThat(requirementSet1).isNotEqualTo(requirementSet2);
        requirementSet1.setId(null);
        assertThat(requirementSet1).isNotEqualTo(requirementSet2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequirementSetDTO.class);
        RequirementSetDTO requirementSetDTO1 = new RequirementSetDTO();
        requirementSetDTO1.setId(1L);
        RequirementSetDTO requirementSetDTO2 = new RequirementSetDTO();
        assertThat(requirementSetDTO1).isNotEqualTo(requirementSetDTO2);
        requirementSetDTO2.setId(requirementSetDTO1.getId());
        assertThat(requirementSetDTO1).isEqualTo(requirementSetDTO2);
        requirementSetDTO2.setId(2L);
        assertThat(requirementSetDTO1).isNotEqualTo(requirementSetDTO2);
        requirementSetDTO1.setId(null);
        assertThat(requirementSetDTO1).isNotEqualTo(requirementSetDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(requirementSetMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(requirementSetMapper.fromId(null)).isNull();
    }
}
