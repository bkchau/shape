package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.Shape;
import com.shape.domain.ShapeCategory;
import com.shape.domain.User;
import com.shape.domain.RequirementSet;
import com.shape.repository.ShapeRepository;
import com.shape.service.ShapeService;
import com.shape.service.dto.ShapeDTO;
import com.shape.service.mapper.ShapeMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ShapeResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class ShapeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Float DEFAULT_AREA = 1F;
    private static final Float UPDATED_AREA = 2F;

    private static final LocalDate DEFAULT_CREATE_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATE_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATE_AT = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private ShapeRepository shapeRepository;

    @Autowired
    private ShapeMapper shapeMapper;

    @Autowired
    private ShapeService shapeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShapeMockMvc;

    private Shape shape;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShapeResource shapeResource = new ShapeResource(shapeService);
        this.restShapeMockMvc = MockMvcBuilders.standaloneSetup(shapeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shape createEntity(EntityManager em) {
        Shape shape = new Shape()
            .name(DEFAULT_NAME)
            .area(DEFAULT_AREA)
            .createAt(DEFAULT_CREATE_AT)
            .updateAt(DEFAULT_UPDATE_AT)
            .isDeleted(DEFAULT_IS_DELETED);
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        shape.setShapeCategory(shapeCategory);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        shape.setCreateBy(user);
        // Add required entity
        RequirementSet requirementSet;
        if (TestUtil.findAll(em, RequirementSet.class).isEmpty()) {
            requirementSet = RequirementSetResourceIT.createEntity(em);
            em.persist(requirementSet);
            em.flush();
        } else {
            requirementSet = TestUtil.findAll(em, RequirementSet.class).get(0);
        }
        shape.setRequirementSet(requirementSet);
        return shape;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shape createUpdatedEntity(EntityManager em) {
        Shape shape = new Shape()
            .name(UPDATED_NAME)
            .area(UPDATED_AREA)
            .createAt(UPDATED_CREATE_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .isDeleted(UPDATED_IS_DELETED);
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createUpdatedEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        shape.setShapeCategory(shapeCategory);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        shape.setCreateBy(user);
        // Add required entity
        RequirementSet requirementSet;
        if (TestUtil.findAll(em, RequirementSet.class).isEmpty()) {
            requirementSet = RequirementSetResourceIT.createUpdatedEntity(em);
            em.persist(requirementSet);
            em.flush();
        } else {
            requirementSet = TestUtil.findAll(em, RequirementSet.class).get(0);
        }
        shape.setRequirementSet(requirementSet);
        return shape;
    }

    @BeforeEach
    public void initTest() {
        shape = createEntity(em);
    }

    @Test
    @Transactional
    public void createShape() throws Exception {
        int databaseSizeBeforeCreate = shapeRepository.findAll().size();

        // Create the Shape
        ShapeDTO shapeDTO = shapeMapper.toDto(shape);
        restShapeMockMvc.perform(post("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isCreated());

        // Validate the Shape in the database
        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeCreate + 1);
        Shape testShape = shapeList.get(shapeList.size() - 1);
        assertThat(testShape.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testShape.getArea()).isEqualTo(DEFAULT_AREA);
        assertThat(testShape.getCreateAt()).isEqualTo(DEFAULT_CREATE_AT);
        assertThat(testShape.getUpdateAt()).isEqualTo(DEFAULT_UPDATE_AT);
        assertThat(testShape.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createShapeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shapeRepository.findAll().size();

        // Create the Shape with an existing ID
        shape.setId(1L);
        ShapeDTO shapeDTO = shapeMapper.toDto(shape);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShapeMockMvc.perform(post("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shape in the database
        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = shapeRepository.findAll().size();
        // set the field null
        shape.setName(null);

        // Create the Shape, which fails.
        ShapeDTO shapeDTO = shapeMapper.toDto(shape);

        restShapeMockMvc.perform(post("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isBadRequest());

        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreateAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = shapeRepository.findAll().size();
        // set the field null
        shape.setCreateAt(null);

        // Create the Shape, which fails.
        ShapeDTO shapeDTO = shapeMapper.toDto(shape);

        restShapeMockMvc.perform(post("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isBadRequest());

        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShapes() throws Exception {
        // Initialize the database
        shapeRepository.saveAndFlush(shape);

        // Get all the shapeList
        restShapeMockMvc.perform(get("/api/shapes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shape.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].area").value(hasItem(DEFAULT_AREA.doubleValue())))
            .andExpect(jsonPath("$.[*].createAt").value(hasItem(DEFAULT_CREATE_AT.toString())))
            .andExpect(jsonPath("$.[*].updateAt").value(hasItem(DEFAULT_UPDATE_AT.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getShape() throws Exception {
        // Initialize the database
        shapeRepository.saveAndFlush(shape);

        // Get the shape
        restShapeMockMvc.perform(get("/api/shapes/{id}", shape.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shape.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.area").value(DEFAULT_AREA.doubleValue()))
            .andExpect(jsonPath("$.createAt").value(DEFAULT_CREATE_AT.toString()))
            .andExpect(jsonPath("$.updateAt").value(DEFAULT_UPDATE_AT.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingShape() throws Exception {
        // Get the shape
        restShapeMockMvc.perform(get("/api/shapes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShape() throws Exception {
        // Initialize the database
        shapeRepository.saveAndFlush(shape);

        int databaseSizeBeforeUpdate = shapeRepository.findAll().size();

        // Update the shape
        Shape updatedShape = shapeRepository.findById(shape.getId()).get();
        // Disconnect from session so that the updates on updatedShape are not directly saved in db
        em.detach(updatedShape);
        updatedShape
            .name(UPDATED_NAME)
            .area(UPDATED_AREA)
            .createAt(UPDATED_CREATE_AT)
            .updateAt(UPDATED_UPDATE_AT)
            .isDeleted(UPDATED_IS_DELETED);
        ShapeDTO shapeDTO = shapeMapper.toDto(updatedShape);

        restShapeMockMvc.perform(put("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isOk());

        // Validate the Shape in the database
        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeUpdate);
        Shape testShape = shapeList.get(shapeList.size() - 1);
        assertThat(testShape.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testShape.getArea()).isEqualTo(UPDATED_AREA);
        assertThat(testShape.getCreateAt()).isEqualTo(UPDATED_CREATE_AT);
        assertThat(testShape.getUpdateAt()).isEqualTo(UPDATED_UPDATE_AT);
        assertThat(testShape.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingShape() throws Exception {
        int databaseSizeBeforeUpdate = shapeRepository.findAll().size();

        // Create the Shape
        ShapeDTO shapeDTO = shapeMapper.toDto(shape);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShapeMockMvc.perform(put("/api/shapes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shape in the database
        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShape() throws Exception {
        // Initialize the database
        shapeRepository.saveAndFlush(shape);

        int databaseSizeBeforeDelete = shapeRepository.findAll().size();

        // Delete the shape
        restShapeMockMvc.perform(delete("/api/shapes/{id}", shape.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Shape> shapeList = shapeRepository.findAll();
        assertThat(shapeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Shape.class);
        Shape shape1 = new Shape();
        shape1.setId(1L);
        Shape shape2 = new Shape();
        shape2.setId(shape1.getId());
        assertThat(shape1).isEqualTo(shape2);
        shape2.setId(2L);
        assertThat(shape1).isNotEqualTo(shape2);
        shape1.setId(null);
        assertThat(shape1).isNotEqualTo(shape2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShapeDTO.class);
        ShapeDTO shapeDTO1 = new ShapeDTO();
        shapeDTO1.setId(1L);
        ShapeDTO shapeDTO2 = new ShapeDTO();
        assertThat(shapeDTO1).isNotEqualTo(shapeDTO2);
        shapeDTO2.setId(shapeDTO1.getId());
        assertThat(shapeDTO1).isEqualTo(shapeDTO2);
        shapeDTO2.setId(2L);
        assertThat(shapeDTO1).isNotEqualTo(shapeDTO2);
        shapeDTO1.setId(null);
        assertThat(shapeDTO1).isNotEqualTo(shapeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shapeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shapeMapper.fromId(null)).isNull();
    }
}
