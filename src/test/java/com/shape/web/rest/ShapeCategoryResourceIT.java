package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.ShapeCategory;
import com.shape.repository.ShapeCategoryRepository;
import com.shape.service.ShapeCategoryService;
import com.shape.service.dto.ShapeCategoryDTO;
import com.shape.service.mapper.ShapeCategoryMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ShapeCategoryResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class ShapeCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ShapeCategoryRepository shapeCategoryRepository;

    @Autowired
    private ShapeCategoryMapper shapeCategoryMapper;

    @Autowired
    private ShapeCategoryService shapeCategoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShapeCategoryMockMvc;

    private ShapeCategory shapeCategory;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShapeCategoryResource shapeCategoryResource = new ShapeCategoryResource(shapeCategoryService);
        this.restShapeCategoryMockMvc = MockMvcBuilders.standaloneSetup(shapeCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShapeCategory createEntity(EntityManager em) {
        ShapeCategory shapeCategory = new ShapeCategory()
            .name(DEFAULT_NAME);
        return shapeCategory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShapeCategory createUpdatedEntity(EntityManager em) {
        ShapeCategory shapeCategory = new ShapeCategory()
            .name(UPDATED_NAME);
        return shapeCategory;
    }

    @BeforeEach
    public void initTest() {
        shapeCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createShapeCategory() throws Exception {
        int databaseSizeBeforeCreate = shapeCategoryRepository.findAll().size();

        // Create the ShapeCategory
        ShapeCategoryDTO shapeCategoryDTO = shapeCategoryMapper.toDto(shapeCategory);
        restShapeCategoryMockMvc.perform(post("/api/shape-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the ShapeCategory in the database
        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        ShapeCategory testShapeCategory = shapeCategoryList.get(shapeCategoryList.size() - 1);
        assertThat(testShapeCategory.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createShapeCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shapeCategoryRepository.findAll().size();

        // Create the ShapeCategory with an existing ID
        shapeCategory.setId(1L);
        ShapeCategoryDTO shapeCategoryDTO = shapeCategoryMapper.toDto(shapeCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShapeCategoryMockMvc.perform(post("/api/shape-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShapeCategory in the database
        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = shapeCategoryRepository.findAll().size();
        // set the field null
        shapeCategory.setName(null);

        // Create the ShapeCategory, which fails.
        ShapeCategoryDTO shapeCategoryDTO = shapeCategoryMapper.toDto(shapeCategory);

        restShapeCategoryMockMvc.perform(post("/api/shape-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeCategoryDTO)))
            .andExpect(status().isBadRequest());

        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShapeCategories() throws Exception {
        // Initialize the database
        shapeCategoryRepository.saveAndFlush(shapeCategory);

        // Get all the shapeCategoryList
        restShapeCategoryMockMvc.perform(get("/api/shape-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shapeCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getShapeCategory() throws Exception {
        // Initialize the database
        shapeCategoryRepository.saveAndFlush(shapeCategory);

        // Get the shapeCategory
        restShapeCategoryMockMvc.perform(get("/api/shape-categories/{id}", shapeCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shapeCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingShapeCategory() throws Exception {
        // Get the shapeCategory
        restShapeCategoryMockMvc.perform(get("/api/shape-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShapeCategory() throws Exception {
        // Initialize the database
        shapeCategoryRepository.saveAndFlush(shapeCategory);

        int databaseSizeBeforeUpdate = shapeCategoryRepository.findAll().size();

        // Update the shapeCategory
        ShapeCategory updatedShapeCategory = shapeCategoryRepository.findById(shapeCategory.getId()).get();
        // Disconnect from session so that the updates on updatedShapeCategory are not directly saved in db
        em.detach(updatedShapeCategory);
        updatedShapeCategory
            .name(UPDATED_NAME);
        ShapeCategoryDTO shapeCategoryDTO = shapeCategoryMapper.toDto(updatedShapeCategory);

        restShapeCategoryMockMvc.perform(put("/api/shape-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the ShapeCategory in the database
        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeUpdate);
        ShapeCategory testShapeCategory = shapeCategoryList.get(shapeCategoryList.size() - 1);
        assertThat(testShapeCategory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingShapeCategory() throws Exception {
        int databaseSizeBeforeUpdate = shapeCategoryRepository.findAll().size();

        // Create the ShapeCategory
        ShapeCategoryDTO shapeCategoryDTO = shapeCategoryMapper.toDto(shapeCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShapeCategoryMockMvc.perform(put("/api/shape-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shapeCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShapeCategory in the database
        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShapeCategory() throws Exception {
        // Initialize the database
        shapeCategoryRepository.saveAndFlush(shapeCategory);

        int databaseSizeBeforeDelete = shapeCategoryRepository.findAll().size();

        // Delete the shapeCategory
        restShapeCategoryMockMvc.perform(delete("/api/shape-categories/{id}", shapeCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShapeCategory> shapeCategoryList = shapeCategoryRepository.findAll();
        assertThat(shapeCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShapeCategory.class);
        ShapeCategory shapeCategory1 = new ShapeCategory();
        shapeCategory1.setId(1L);
        ShapeCategory shapeCategory2 = new ShapeCategory();
        shapeCategory2.setId(shapeCategory1.getId());
        assertThat(shapeCategory1).isEqualTo(shapeCategory2);
        shapeCategory2.setId(2L);
        assertThat(shapeCategory1).isNotEqualTo(shapeCategory2);
        shapeCategory1.setId(null);
        assertThat(shapeCategory1).isNotEqualTo(shapeCategory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShapeCategoryDTO.class);
        ShapeCategoryDTO shapeCategoryDTO1 = new ShapeCategoryDTO();
        shapeCategoryDTO1.setId(1L);
        ShapeCategoryDTO shapeCategoryDTO2 = new ShapeCategoryDTO();
        assertThat(shapeCategoryDTO1).isNotEqualTo(shapeCategoryDTO2);
        shapeCategoryDTO2.setId(shapeCategoryDTO1.getId());
        assertThat(shapeCategoryDTO1).isEqualTo(shapeCategoryDTO2);
        shapeCategoryDTO2.setId(2L);
        assertThat(shapeCategoryDTO1).isNotEqualTo(shapeCategoryDTO2);
        shapeCategoryDTO1.setId(null);
        assertThat(shapeCategoryDTO1).isNotEqualTo(shapeCategoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shapeCategoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shapeCategoryMapper.fromId(null)).isNull();
    }
}
