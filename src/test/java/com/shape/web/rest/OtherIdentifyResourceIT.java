package com.shape.web.rest;

import com.shape.ShapeApp;
import com.shape.domain.OtherIdentify;
import com.shape.domain.ShapeCategory;
import com.shape.repository.OtherIdentifyRepository;
import com.shape.service.OtherIdentifyService;
import com.shape.service.dto.OtherIdentifyDTO;
import com.shape.service.mapper.OtherIdentifyMapper;
import com.shape.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.shape.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link OtherIdentifyResource} REST controller.
 */
@SpringBootTest(classes = ShapeApp.class)
public class OtherIdentifyResourceIT {

    @Autowired
    private OtherIdentifyRepository otherIdentifyRepository;

    @Autowired
    private OtherIdentifyMapper otherIdentifyMapper;

    @Autowired
    private OtherIdentifyService otherIdentifyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOtherIdentifyMockMvc;

    private OtherIdentify otherIdentify;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OtherIdentifyResource otherIdentifyResource = new OtherIdentifyResource(otherIdentifyService);
        this.restOtherIdentifyMockMvc = MockMvcBuilders.standaloneSetup(otherIdentifyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherIdentify createEntity(EntityManager em) {
        OtherIdentify otherIdentify = new OtherIdentify();
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        otherIdentify.setShapeCategory(shapeCategory);
        // Add required entity
        otherIdentify.setOtherIdentify(shapeCategory);
        return otherIdentify;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherIdentify createUpdatedEntity(EntityManager em) {
        OtherIdentify otherIdentify = new OtherIdentify();
        // Add required entity
        ShapeCategory shapeCategory;
        if (TestUtil.findAll(em, ShapeCategory.class).isEmpty()) {
            shapeCategory = ShapeCategoryResourceIT.createUpdatedEntity(em);
            em.persist(shapeCategory);
            em.flush();
        } else {
            shapeCategory = TestUtil.findAll(em, ShapeCategory.class).get(0);
        }
        otherIdentify.setShapeCategory(shapeCategory);
        // Add required entity
        otherIdentify.setOtherIdentify(shapeCategory);
        return otherIdentify;
    }

    @BeforeEach
    public void initTest() {
        otherIdentify = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtherIdentify() throws Exception {
        int databaseSizeBeforeCreate = otherIdentifyRepository.findAll().size();

        // Create the OtherIdentify
        OtherIdentifyDTO otherIdentifyDTO = otherIdentifyMapper.toDto(otherIdentify);
        restOtherIdentifyMockMvc.perform(post("/api/other-identifies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherIdentifyDTO)))
            .andExpect(status().isCreated());

        // Validate the OtherIdentify in the database
        List<OtherIdentify> otherIdentifyList = otherIdentifyRepository.findAll();
        assertThat(otherIdentifyList).hasSize(databaseSizeBeforeCreate + 1);
        OtherIdentify testOtherIdentify = otherIdentifyList.get(otherIdentifyList.size() - 1);
    }

    @Test
    @Transactional
    public void createOtherIdentifyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otherIdentifyRepository.findAll().size();

        // Create the OtherIdentify with an existing ID
        otherIdentify.setId(1L);
        OtherIdentifyDTO otherIdentifyDTO = otherIdentifyMapper.toDto(otherIdentify);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherIdentifyMockMvc.perform(post("/api/other-identifies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherIdentifyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherIdentify in the database
        List<OtherIdentify> otherIdentifyList = otherIdentifyRepository.findAll();
        assertThat(otherIdentifyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOtherIdentifies() throws Exception {
        // Initialize the database
        otherIdentifyRepository.saveAndFlush(otherIdentify);

        // Get all the otherIdentifyList
        restOtherIdentifyMockMvc.perform(get("/api/other-identifies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherIdentify.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getOtherIdentify() throws Exception {
        // Initialize the database
        otherIdentifyRepository.saveAndFlush(otherIdentify);

        // Get the otherIdentify
        restOtherIdentifyMockMvc.perform(get("/api/other-identifies/{id}", otherIdentify.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(otherIdentify.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOtherIdentify() throws Exception {
        // Get the otherIdentify
        restOtherIdentifyMockMvc.perform(get("/api/other-identifies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtherIdentify() throws Exception {
        // Initialize the database
        otherIdentifyRepository.saveAndFlush(otherIdentify);

        int databaseSizeBeforeUpdate = otherIdentifyRepository.findAll().size();

        // Update the otherIdentify
        OtherIdentify updatedOtherIdentify = otherIdentifyRepository.findById(otherIdentify.getId()).get();
        // Disconnect from session so that the updates on updatedOtherIdentify are not directly saved in db
        em.detach(updatedOtherIdentify);
        OtherIdentifyDTO otherIdentifyDTO = otherIdentifyMapper.toDto(updatedOtherIdentify);

        restOtherIdentifyMockMvc.perform(put("/api/other-identifies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherIdentifyDTO)))
            .andExpect(status().isOk());

        // Validate the OtherIdentify in the database
        List<OtherIdentify> otherIdentifyList = otherIdentifyRepository.findAll();
        assertThat(otherIdentifyList).hasSize(databaseSizeBeforeUpdate);
        OtherIdentify testOtherIdentify = otherIdentifyList.get(otherIdentifyList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingOtherIdentify() throws Exception {
        int databaseSizeBeforeUpdate = otherIdentifyRepository.findAll().size();

        // Create the OtherIdentify
        OtherIdentifyDTO otherIdentifyDTO = otherIdentifyMapper.toDto(otherIdentify);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherIdentifyMockMvc.perform(put("/api/other-identifies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otherIdentifyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherIdentify in the database
        List<OtherIdentify> otherIdentifyList = otherIdentifyRepository.findAll();
        assertThat(otherIdentifyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOtherIdentify() throws Exception {
        // Initialize the database
        otherIdentifyRepository.saveAndFlush(otherIdentify);

        int databaseSizeBeforeDelete = otherIdentifyRepository.findAll().size();

        // Delete the otherIdentify
        restOtherIdentifyMockMvc.perform(delete("/api/other-identifies/{id}", otherIdentify.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherIdentify> otherIdentifyList = otherIdentifyRepository.findAll();
        assertThat(otherIdentifyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherIdentify.class);
        OtherIdentify otherIdentify1 = new OtherIdentify();
        otherIdentify1.setId(1L);
        OtherIdentify otherIdentify2 = new OtherIdentify();
        otherIdentify2.setId(otherIdentify1.getId());
        assertThat(otherIdentify1).isEqualTo(otherIdentify2);
        otherIdentify2.setId(2L);
        assertThat(otherIdentify1).isNotEqualTo(otherIdentify2);
        otherIdentify1.setId(null);
        assertThat(otherIdentify1).isNotEqualTo(otherIdentify2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherIdentifyDTO.class);
        OtherIdentifyDTO otherIdentifyDTO1 = new OtherIdentifyDTO();
        otherIdentifyDTO1.setId(1L);
        OtherIdentifyDTO otherIdentifyDTO2 = new OtherIdentifyDTO();
        assertThat(otherIdentifyDTO1).isNotEqualTo(otherIdentifyDTO2);
        otherIdentifyDTO2.setId(otherIdentifyDTO1.getId());
        assertThat(otherIdentifyDTO1).isEqualTo(otherIdentifyDTO2);
        otherIdentifyDTO2.setId(2L);
        assertThat(otherIdentifyDTO1).isNotEqualTo(otherIdentifyDTO2);
        otherIdentifyDTO1.setId(null);
        assertThat(otherIdentifyDTO1).isNotEqualTo(otherIdentifyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(otherIdentifyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(otherIdentifyMapper.fromId(null)).isNull();
    }
}
