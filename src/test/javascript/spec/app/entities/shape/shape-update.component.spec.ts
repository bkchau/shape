/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeUpdateComponent } from 'app/entities/shape/shape-update.component';
import { ShapeService } from 'app/entities/shape/shape.service';
import { Shape } from 'app/shared/model/shape.model';

describe('Component Tests', () => {
  describe('Shape Management Update Component', () => {
    let comp: ShapeUpdateComponent;
    let fixture: ComponentFixture<ShapeUpdateComponent>;
    let service: ShapeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShapeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShapeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Shape(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Shape();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
