/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeDetailComponent } from 'app/entities/shape/shape-detail.component';
import { Shape } from 'app/shared/model/shape.model';

describe('Component Tests', () => {
  describe('Shape Management Detail Component', () => {
    let comp: ShapeDetailComponent;
    let fixture: ComponentFixture<ShapeDetailComponent>;
    const route = ({ data: of({ shape: new Shape(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShapeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shape).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
