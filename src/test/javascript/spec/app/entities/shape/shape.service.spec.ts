/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ShapeService } from 'app/entities/shape/shape.service';
import { IShape, Shape } from 'app/shared/model/shape.model';

describe('Service Tests', () => {
  describe('Shape Service', () => {
    let injector: TestBed;
    let service: ShapeService;
    let httpMock: HttpTestingController;
    let elemDefault: IShape;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(ShapeService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Shape(0, 'AAAAAAA', 0, currentDate, currentDate, false);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            createAt: currentDate.format(DATE_FORMAT),
            updateAt: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Shape', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createAt: currentDate.format(DATE_FORMAT),
            updateAt: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createAt: currentDate,
            updateAt: currentDate
          },
          returnedFromService
        );
        service
          .create(new Shape(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Shape', async () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            area: 1,
            createAt: currentDate.format(DATE_FORMAT),
            updateAt: currentDate.format(DATE_FORMAT),
            isDeleted: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createAt: currentDate,
            updateAt: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Shape', async () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            area: 1,
            createAt: currentDate.format(DATE_FORMAT),
            updateAt: currentDate.format(DATE_FORMAT),
            isDeleted: true
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createAt: currentDate,
            updateAt: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Shape', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
