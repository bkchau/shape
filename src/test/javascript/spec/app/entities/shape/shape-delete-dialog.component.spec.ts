/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ShapeTestModule } from '../../../test.module';
import { ShapeDeleteDialogComponent } from 'app/entities/shape/shape-delete-dialog.component';
import { ShapeService } from 'app/entities/shape/shape.service';

describe('Component Tests', () => {
  describe('Shape Management Delete Component', () => {
    let comp: ShapeDeleteDialogComponent;
    let fixture: ComponentFixture<ShapeDeleteDialogComponent>;
    let service: ShapeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeDeleteDialogComponent]
      })
        .overrideTemplate(ShapeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
