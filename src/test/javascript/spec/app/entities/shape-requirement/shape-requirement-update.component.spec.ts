/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeRequirementUpdateComponent } from 'app/entities/shape-requirement/shape-requirement-update.component';
import { ShapeRequirementService } from 'app/entities/shape-requirement/shape-requirement.service';
import { ShapeRequirement } from 'app/shared/model/shape-requirement.model';

describe('Component Tests', () => {
  describe('ShapeRequirement Management Update Component', () => {
    let comp: ShapeRequirementUpdateComponent;
    let fixture: ComponentFixture<ShapeRequirementUpdateComponent>;
    let service: ShapeRequirementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeRequirementUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShapeRequirementUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShapeRequirementUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeRequirementService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShapeRequirement(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShapeRequirement();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
