/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeRequirementDetailComponent } from 'app/entities/shape-requirement/shape-requirement-detail.component';
import { ShapeRequirement } from 'app/shared/model/shape-requirement.model';

describe('Component Tests', () => {
  describe('ShapeRequirement Management Detail Component', () => {
    let comp: ShapeRequirementDetailComponent;
    let fixture: ComponentFixture<ShapeRequirementDetailComponent>;
    const route = ({ data: of({ shapeRequirement: new ShapeRequirement(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeRequirementDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShapeRequirementDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeRequirementDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shapeRequirement).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
