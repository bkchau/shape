/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ShapeTestModule } from '../../../test.module';
import { ShapeRequirementDeleteDialogComponent } from 'app/entities/shape-requirement/shape-requirement-delete-dialog.component';
import { ShapeRequirementService } from 'app/entities/shape-requirement/shape-requirement.service';

describe('Component Tests', () => {
  describe('ShapeRequirement Management Delete Component', () => {
    let comp: ShapeRequirementDeleteDialogComponent;
    let fixture: ComponentFixture<ShapeRequirementDeleteDialogComponent>;
    let service: ShapeRequirementService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeRequirementDeleteDialogComponent]
      })
        .overrideTemplate(ShapeRequirementDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeRequirementDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeRequirementService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
