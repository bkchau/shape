/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { AreaFormulaUpdateComponent } from 'app/entities/area-formula/area-formula-update.component';
import { AreaFormulaService } from 'app/entities/area-formula/area-formula.service';
import { AreaFormula } from 'app/shared/model/area-formula.model';

describe('Component Tests', () => {
  describe('AreaFormula Management Update Component', () => {
    let comp: AreaFormulaUpdateComponent;
    let fixture: ComponentFixture<AreaFormulaUpdateComponent>;
    let service: AreaFormulaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [AreaFormulaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AreaFormulaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AreaFormulaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AreaFormulaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AreaFormula(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AreaFormula();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
