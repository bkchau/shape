/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { AreaFormulaDetailComponent } from 'app/entities/area-formula/area-formula-detail.component';
import { AreaFormula } from 'app/shared/model/area-formula.model';

describe('Component Tests', () => {
  describe('AreaFormula Management Detail Component', () => {
    let comp: AreaFormulaDetailComponent;
    let fixture: ComponentFixture<AreaFormulaDetailComponent>;
    const route = ({ data: of({ areaFormula: new AreaFormula(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [AreaFormulaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AreaFormulaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AreaFormulaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.areaFormula).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
