/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ShapeTestModule } from '../../../test.module';
import { AreaFormulaDeleteDialogComponent } from 'app/entities/area-formula/area-formula-delete-dialog.component';
import { AreaFormulaService } from 'app/entities/area-formula/area-formula.service';

describe('Component Tests', () => {
  describe('AreaFormula Management Delete Component', () => {
    let comp: AreaFormulaDeleteDialogComponent;
    let fixture: ComponentFixture<AreaFormulaDeleteDialogComponent>;
    let service: AreaFormulaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [AreaFormulaDeleteDialogComponent]
      })
        .overrideTemplate(AreaFormulaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AreaFormulaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AreaFormulaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
