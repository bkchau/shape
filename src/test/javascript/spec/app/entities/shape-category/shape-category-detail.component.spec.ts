/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeCategoryDetailComponent } from 'app/entities/shape-category/shape-category-detail.component';
import { ShapeCategory } from 'app/shared/model/shape-category.model';

describe('Component Tests', () => {
  describe('ShapeCategory Management Detail Component', () => {
    let comp: ShapeCategoryDetailComponent;
    let fixture: ComponentFixture<ShapeCategoryDetailComponent>;
    const route = ({ data: of({ shapeCategory: new ShapeCategory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeCategoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShapeCategoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeCategoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shapeCategory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
