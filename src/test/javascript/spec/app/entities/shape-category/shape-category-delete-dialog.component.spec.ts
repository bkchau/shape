/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ShapeTestModule } from '../../../test.module';
import { ShapeCategoryDeleteDialogComponent } from 'app/entities/shape-category/shape-category-delete-dialog.component';
import { ShapeCategoryService } from 'app/entities/shape-category/shape-category.service';

describe('Component Tests', () => {
  describe('ShapeCategory Management Delete Component', () => {
    let comp: ShapeCategoryDeleteDialogComponent;
    let fixture: ComponentFixture<ShapeCategoryDeleteDialogComponent>;
    let service: ShapeCategoryService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeCategoryDeleteDialogComponent]
      })
        .overrideTemplate(ShapeCategoryDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShapeCategoryDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeCategoryService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
