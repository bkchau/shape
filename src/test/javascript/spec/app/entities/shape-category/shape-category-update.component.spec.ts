/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { ShapeCategoryUpdateComponent } from 'app/entities/shape-category/shape-category-update.component';
import { ShapeCategoryService } from 'app/entities/shape-category/shape-category.service';
import { ShapeCategory } from 'app/shared/model/shape-category.model';

describe('Component Tests', () => {
  describe('ShapeCategory Management Update Component', () => {
    let comp: ShapeCategoryUpdateComponent;
    let fixture: ComponentFixture<ShapeCategoryUpdateComponent>;
    let service: ShapeCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [ShapeCategoryUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShapeCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShapeCategoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShapeCategoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShapeCategory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShapeCategory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
