/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { OtherIdentifyDetailComponent } from 'app/entities/other-identify/other-identify-detail.component';
import { OtherIdentify } from 'app/shared/model/other-identify.model';

describe('Component Tests', () => {
  describe('OtherIdentify Management Detail Component', () => {
    let comp: OtherIdentifyDetailComponent;
    let fixture: ComponentFixture<OtherIdentifyDetailComponent>;
    const route = ({ data: of({ otherIdentify: new OtherIdentify(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [OtherIdentifyDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OtherIdentifyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherIdentifyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.otherIdentify).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
