/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ShapeTestModule } from '../../../test.module';
import { OtherIdentifyDeleteDialogComponent } from 'app/entities/other-identify/other-identify-delete-dialog.component';
import { OtherIdentifyService } from 'app/entities/other-identify/other-identify.service';

describe('Component Tests', () => {
  describe('OtherIdentify Management Delete Component', () => {
    let comp: OtherIdentifyDeleteDialogComponent;
    let fixture: ComponentFixture<OtherIdentifyDeleteDialogComponent>;
    let service: OtherIdentifyService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [OtherIdentifyDeleteDialogComponent]
      })
        .overrideTemplate(OtherIdentifyDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtherIdentifyDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherIdentifyService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
