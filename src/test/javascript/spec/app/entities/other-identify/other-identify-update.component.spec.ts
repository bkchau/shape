/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ShapeTestModule } from '../../../test.module';
import { OtherIdentifyUpdateComponent } from 'app/entities/other-identify/other-identify-update.component';
import { OtherIdentifyService } from 'app/entities/other-identify/other-identify.service';
import { OtherIdentify } from 'app/shared/model/other-identify.model';

describe('Component Tests', () => {
  describe('OtherIdentify Management Update Component', () => {
    let comp: OtherIdentifyUpdateComponent;
    let fixture: ComponentFixture<OtherIdentifyUpdateComponent>;
    let service: OtherIdentifyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShapeTestModule],
        declarations: [OtherIdentifyUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OtherIdentifyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OtherIdentifyUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtherIdentifyService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherIdentify(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OtherIdentify();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
