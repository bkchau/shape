package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RequirementSet.
 */
@Entity
@Table(name = "requirement_set")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RequirementSet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(optional = false)
    @NotNull
    private ShapeCategory shapeCategory;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "requirement_set_requirements",
               joinColumns = @JoinColumn(name = "requirement_set_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "requirements_id", referencedColumnName = "id"))
    private Set<Requirement> requirements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public RequirementSet name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShapeCategory getShapeCategory() {
        return shapeCategory;
    }

    public RequirementSet shapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
        return this;
    }

    public void setShapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
    }

    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public RequirementSet requirements(Set<Requirement> requirements) {
        this.requirements = requirements;
        return this;
    }

    public RequirementSet addRequirements(Requirement requirement) {
        this.requirements.add(requirement);
        requirement.getRequirementSets().add(this);
        return this;
    }

    public RequirementSet removeRequirements(Requirement requirement) {
        this.requirements.remove(requirement);
        requirement.getRequirementSets().remove(this);
        return this;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementSet)) {
            return false;
        }
        return id != null && id.equals(((RequirementSet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RequirementSet{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
