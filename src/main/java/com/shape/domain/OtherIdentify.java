package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A OtherIdentify.
 */
@Entity
@Table(name = "other_identify")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OtherIdentify implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    private ShapeCategory shapeCategory;

    @ManyToOne(optional = false)
    @NotNull
    private ShapeCategory otherIdentify;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShapeCategory getShapeCategory() {
        return shapeCategory;
    }

    public OtherIdentify shapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
        return this;
    }

    public void setShapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
    }

    public ShapeCategory getOtherIdentify() {
        return otherIdentify;
    }

    public OtherIdentify otherIdentify(ShapeCategory shapeCategory) {
        this.otherIdentify = shapeCategory;
        return this;
    }

    public void setOtherIdentify(ShapeCategory shapeCategory) {
        this.otherIdentify = shapeCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherIdentify)) {
            return false;
        }
        return id != null && id.equals(((OtherIdentify) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OtherIdentify{" +
            "id=" + getId() +
            "}";
    }
}
