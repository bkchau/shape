package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Requirement.
 */
@Entity
@Table(name = "requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Requirement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "formula_key", nullable = false)
    private String formulaKey;

    @ManyToMany(mappedBy = "requirements")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<RequirementSet> requirementSets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Requirement name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormulaKey() {
        return formulaKey;
    }

    public Requirement formulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
        return this;
    }

    public void setFormulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
    }

    public Set<RequirementSet> getRequirementSets() {
        return requirementSets;
    }

    public Requirement requirementSets(Set<RequirementSet> requirementSets) {
        this.requirementSets = requirementSets;
        return this;
    }

    public Requirement addRequirementSets(RequirementSet requirementSet) {
        this.requirementSets.add(requirementSet);
        requirementSet.getRequirements().add(this);
        return this;
    }

    public Requirement removeRequirementSets(RequirementSet requirementSet) {
        this.requirementSets.remove(requirementSet);
        requirementSet.getRequirements().remove(this);
        return this;
    }

    public void setRequirementSets(Set<RequirementSet> requirementSets) {
        this.requirementSets = requirementSets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Requirement)) {
            return false;
        }
        return id != null && id.equals(((Requirement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Requirement{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", formulaKey='" + getFormulaKey() + "'" +
            "}";
    }
}
