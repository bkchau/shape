package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AreaFormula.
 */
@Entity
@Table(name = "area_formula")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AreaFormula implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "operator")
    private String operator;

    @Column(name = "formula_key")
    private String formulaKey;

    @ManyToOne(optional = false)
    @NotNull
    private RequirementSet requirementSet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public AreaFormula type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public AreaFormula operator(String operator) {
        this.operator = operator;
        return this;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFormulaKey() {
        return formulaKey;
    }

    public AreaFormula formulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
        return this;
    }

    public void setFormulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
    }

    public RequirementSet getRequirementSet() {
        return requirementSet;
    }

    public AreaFormula requirementSet(RequirementSet requirementSet) {
        this.requirementSet = requirementSet;
        return this;
    }

    public void setRequirementSet(RequirementSet requirementSet) {
        this.requirementSet = requirementSet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AreaFormula)) {
            return false;
        }
        return id != null && id.equals(((AreaFormula) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AreaFormula{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", operator='" + getOperator() + "'" +
            ", formulaKey='" + getFormulaKey() + "'" +
            "}";
    }
}
