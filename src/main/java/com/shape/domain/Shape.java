package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Shape.
 */
@Entity
@Table(name = "shape")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Shape implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "area")
    private Float area;

    @NotNull
    @Column(name = "create_at", nullable = false)
    private LocalDate createAt;

    @Column(name = "update_at")
    private LocalDate updateAt;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shapes")
    private ShapeCategory shapeCategory;

    @ManyToOne
    @JsonIgnoreProperties("shapes")
    private User kid;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shapes")
    private User createBy;

    @ManyToOne
    @JsonIgnoreProperties("shapes")
    private User updateBy;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shapes")
    private RequirementSet requirementSet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Shape name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getArea() {
        return area;
    }

    public Shape area(Float area) {
        this.area = area;
        return this;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public LocalDate getCreateAt() {
        return createAt;
    }

    public Shape createAt(LocalDate createAt) {
        this.createAt = createAt;
        return this;
    }

    public void setCreateAt(LocalDate createAt) {
        this.createAt = createAt;
    }

    public LocalDate getUpdateAt() {
        return updateAt;
    }

    public Shape updateAt(LocalDate updateAt) {
        this.updateAt = updateAt;
        return this;
    }

    public void setUpdateAt(LocalDate updateAt) {
        this.updateAt = updateAt;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public Shape isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public ShapeCategory getShapeCategory() {
        return shapeCategory;
    }

    public Shape shapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
        return this;
    }

    public void setShapeCategory(ShapeCategory shapeCategory) {
        this.shapeCategory = shapeCategory;
    }

    public User getKid() {
        return kid;
    }

    public Shape kid(User user) {
        this.kid = user;
        return this;
    }

    public void setKid(User user) {
        this.kid = user;
    }

    public User getCreateBy() {
        return createBy;
    }

    public Shape createBy(User user) {
        this.createBy = user;
        return this;
    }

    public void setCreateBy(User user) {
        this.createBy = user;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public Shape updateBy(User user) {
        this.updateBy = user;
        return this;
    }

    public void setUpdateBy(User user) {
        this.updateBy = user;
    }

    public RequirementSet getRequirementSet() {
        return requirementSet;
    }

    public Shape requirementSet(RequirementSet requirementSet) {
        this.requirementSet = requirementSet;
        return this;
    }

    public void setRequirementSet(RequirementSet requirementSet) {
        this.requirementSet = requirementSet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shape)) {
            return false;
        }
        return id != null && id.equals(((Shape) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Shape{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", area=" + getArea() +
            ", createAt='" + getCreateAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
