package com.shape.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ShapeRequirement.
 */
@Entity
@Table(name = "shape_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShapeRequirement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "value", nullable = false)
    private Float value;

    @NotNull
    @Column(name = "unit", nullable = false)
    private String unit;

    @ManyToOne(optional = false)
    @NotNull
    private Shape shape;

    @ManyToOne(optional = false)
    @NotNull
    private Requirement requirement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValue() {
        return value;
    }

    public ShapeRequirement value(Float value) {
        this.value = value;
        return this;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public ShapeRequirement unit(String unit) {
        this.unit = unit;
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Shape getShape() {
        return shape;
    }

    public ShapeRequirement shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public ShapeRequirement requirement(Requirement requirement) {
        this.requirement = requirement;
        return this;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShapeRequirement)) {
            return false;
        }
        return id != null && id.equals(((ShapeRequirement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShapeRequirement{" +
            "id=" + getId() +
            ", value=" + getValue() +
            ", unit='" + getUnit() + "'" +
            "}";
    }
}
