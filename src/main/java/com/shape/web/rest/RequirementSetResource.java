package com.shape.web.rest;

import com.shape.service.RequirementSetService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.RequirementSetDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.RequirementSet}.
 */
@RestController
@RequestMapping("/api")
public class RequirementSetResource {

    private final Logger log = LoggerFactory.getLogger(RequirementSetResource.class);

    private static final String ENTITY_NAME = "requirementSet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequirementSetService requirementSetService;

    public RequirementSetResource(RequirementSetService requirementSetService) {
        this.requirementSetService = requirementSetService;
    }

    /**
     * {@code POST  /requirement-sets} : Create a new requirementSet.
     *
     * @param requirementSetDTO the requirementSetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new requirementSetDTO, or with status {@code 400 (Bad Request)} if the requirementSet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/requirement-sets")
    public ResponseEntity<RequirementSetDTO> createRequirementSet(@Valid @RequestBody RequirementSetDTO requirementSetDTO) throws URISyntaxException {
        log.debug("REST request to save RequirementSet : {}", requirementSetDTO);
        if (requirementSetDTO.getId() != null) {
            throw new BadRequestAlertException("A new requirementSet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequirementSetDTO result = requirementSetService.save(requirementSetDTO);
        return ResponseEntity.created(new URI("/api/requirement-sets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /requirement-sets} : Updates an existing requirementSet.
     *
     * @param requirementSetDTO the requirementSetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requirementSetDTO,
     * or with status {@code 400 (Bad Request)} if the requirementSetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requirementSetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/requirement-sets")
    public ResponseEntity<RequirementSetDTO> updateRequirementSet(@Valid @RequestBody RequirementSetDTO requirementSetDTO) throws URISyntaxException {
        log.debug("REST request to update RequirementSet : {}", requirementSetDTO);
        if (requirementSetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RequirementSetDTO result = requirementSetService.save(requirementSetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requirementSetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /requirement-sets} : get all the requirementSets.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requirementSets in body.
     */
    @GetMapping("/requirement-sets")
    public ResponseEntity<List<RequirementSetDTO>> getAllRequirementSets(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of RequirementSets");
        Page<RequirementSetDTO> page;
        if (eagerload) {
            page = requirementSetService.findAllWithEagerRelationships(pageable);
        } else {
            page = requirementSetService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /requirement-sets/:id} : get the "id" requirementSet.
     *
     * @param id the id of the requirementSetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requirementSetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/requirement-sets/{id}")
    public ResponseEntity<RequirementSetDTO> getRequirementSet(@PathVariable Long id) {
        log.debug("REST request to get RequirementSet : {}", id);
        Optional<RequirementSetDTO> requirementSetDTO = requirementSetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requirementSetDTO);
    }

    /**
     * {@code DELETE  /requirement-sets/:id} : delete the "id" requirementSet.
     *
     * @param id the id of the requirementSetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/requirement-sets/{id}")
    public ResponseEntity<Void> deleteRequirementSet(@PathVariable Long id) {
        log.debug("REST request to delete RequirementSet : {}", id);
        requirementSetService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
