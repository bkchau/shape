package com.shape.web.rest;

import com.shape.service.ShapeRequirementService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.ShapeRequirementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.ShapeRequirement}.
 */
@RestController
@RequestMapping("/api")
public class ShapeRequirementResource {

    private final Logger log = LoggerFactory.getLogger(ShapeRequirementResource.class);

    private static final String ENTITY_NAME = "shapeRequirement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShapeRequirementService shapeRequirementService;

    public ShapeRequirementResource(ShapeRequirementService shapeRequirementService) {
        this.shapeRequirementService = shapeRequirementService;
    }

    /**
     * {@code POST  /shape-requirements} : Create a new shapeRequirement.
     *
     * @param shapeRequirementDTO the shapeRequirementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shapeRequirementDTO, or with status {@code 400 (Bad Request)} if the shapeRequirement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shape-requirements")
    public ResponseEntity<ShapeRequirementDTO> createShapeRequirement(@Valid @RequestBody ShapeRequirementDTO shapeRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save ShapeRequirement : {}", shapeRequirementDTO);
        if (shapeRequirementDTO.getId() != null) {
            throw new BadRequestAlertException("A new shapeRequirement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShapeRequirementDTO result = shapeRequirementService.save(shapeRequirementDTO);
        return ResponseEntity.created(new URI("/api/shape-requirements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shape-requirements} : Updates an existing shapeRequirement.
     *
     * @param shapeRequirementDTO the shapeRequirementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shapeRequirementDTO,
     * or with status {@code 400 (Bad Request)} if the shapeRequirementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shapeRequirementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shape-requirements")
    public ResponseEntity<ShapeRequirementDTO> updateShapeRequirement(@Valid @RequestBody ShapeRequirementDTO shapeRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update ShapeRequirement : {}", shapeRequirementDTO);
        if (shapeRequirementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShapeRequirementDTO result = shapeRequirementService.save(shapeRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shapeRequirementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shape-requirements} : get all the shapeRequirements.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shapeRequirements in body.
     */
    @GetMapping("/shape-requirements")
    public ResponseEntity<List<ShapeRequirementDTO>> getAllShapeRequirements(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ShapeRequirements");
        Page<ShapeRequirementDTO> page = shapeRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shape-requirements/:id} : get the "id" shapeRequirement.
     *
     * @param id the id of the shapeRequirementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shapeRequirementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shape-requirements/{id}")
    public ResponseEntity<ShapeRequirementDTO> getShapeRequirement(@PathVariable Long id) {
        log.debug("REST request to get ShapeRequirement : {}", id);
        Optional<ShapeRequirementDTO> shapeRequirementDTO = shapeRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shapeRequirementDTO);
    }

    /**
     * {@code DELETE  /shape-requirements/:id} : delete the "id" shapeRequirement.
     *
     * @param id the id of the shapeRequirementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shape-requirements/{id}")
    public ResponseEntity<Void> deleteShapeRequirement(@PathVariable Long id) {
        log.debug("REST request to delete ShapeRequirement : {}", id);
        shapeRequirementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
