package com.shape.web.rest;

import com.shape.service.OtherIdentifyService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.OtherIdentifyDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.OtherIdentify}.
 */
@RestController
@RequestMapping("/api")
public class OtherIdentifyResource {

    private final Logger log = LoggerFactory.getLogger(OtherIdentifyResource.class);

    private static final String ENTITY_NAME = "otherIdentify";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherIdentifyService otherIdentifyService;

    public OtherIdentifyResource(OtherIdentifyService otherIdentifyService) {
        this.otherIdentifyService = otherIdentifyService;
    }

    /**
     * {@code POST  /other-identifies} : Create a new otherIdentify.
     *
     * @param otherIdentifyDTO the otherIdentifyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherIdentifyDTO, or with status {@code 400 (Bad Request)} if the otherIdentify has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-identifies")
    public ResponseEntity<OtherIdentifyDTO> createOtherIdentify(@Valid @RequestBody OtherIdentifyDTO otherIdentifyDTO) throws URISyntaxException {
        log.debug("REST request to save OtherIdentify : {}", otherIdentifyDTO);
        if (otherIdentifyDTO.getId() != null) {
            throw new BadRequestAlertException("A new otherIdentify cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherIdentifyDTO result = otherIdentifyService.save(otherIdentifyDTO);
        return ResponseEntity.created(new URI("/api/other-identifies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-identifies} : Updates an existing otherIdentify.
     *
     * @param otherIdentifyDTO the otherIdentifyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherIdentifyDTO,
     * or with status {@code 400 (Bad Request)} if the otherIdentifyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherIdentifyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-identifies")
    public ResponseEntity<OtherIdentifyDTO> updateOtherIdentify(@Valid @RequestBody OtherIdentifyDTO otherIdentifyDTO) throws URISyntaxException {
        log.debug("REST request to update OtherIdentify : {}", otherIdentifyDTO);
        if (otherIdentifyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtherIdentifyDTO result = otherIdentifyService.save(otherIdentifyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherIdentifyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /other-identifies} : get all the otherIdentifies.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherIdentifies in body.
     */
    @GetMapping("/other-identifies")
    public ResponseEntity<List<OtherIdentifyDTO>> getAllOtherIdentifies(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of OtherIdentifies");
        Page<OtherIdentifyDTO> page = otherIdentifyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /other-identifies/:id} : get the "id" otherIdentify.
     *
     * @param id the id of the otherIdentifyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherIdentifyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-identifies/{id}")
    public ResponseEntity<OtherIdentifyDTO> getOtherIdentify(@PathVariable Long id) {
        log.debug("REST request to get OtherIdentify : {}", id);
        Optional<OtherIdentifyDTO> otherIdentifyDTO = otherIdentifyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherIdentifyDTO);
    }

    /**
     * {@code DELETE  /other-identifies/:id} : delete the "id" otherIdentify.
     *
     * @param id the id of the otherIdentifyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-identifies/{id}")
    public ResponseEntity<Void> deleteOtherIdentify(@PathVariable Long id) {
        log.debug("REST request to delete OtherIdentify : {}", id);
        otherIdentifyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
