package com.shape.web.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shape.domain.Authority;
import com.shape.domain.User;
import com.shape.security.AuthoritiesConstants;
import com.shape.security.SecurityUtils;
import com.shape.service.ShapeCategoryService;
import com.shape.service.ShapeService;
import com.shape.service.UserService;
import com.shape.service.dto.ShapeCategoryShapeManagementDTO;
import com.shape.service.dto.ShapeManagementDTO;
import com.shape.web.rest.vm.ShapeVM;

import io.github.jhipster.web.util.HeaderUtil;

/**
 * REST controller for managing {@link com.shape.domain.ShapeCategory}.
 */
@RestController
@RequestMapping("/api/shape-management")
public class ShapeManagementResource {

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShapeCategoryService shapeCategoryService;
    
    private final ShapeService shapeService;
    
    private final UserService userService;

    public ShapeManagementResource(ShapeCategoryService shapeCategoryService, ShapeService shapeService, UserService userService) {
        this.shapeCategoryService = shapeCategoryService;
        this.shapeService = shapeService;
        this.userService = userService;
    }

    @GetMapping("/get-all-shape-category")
    public ResponseEntity<List<ShapeCategoryShapeManagementDTO>> getAllShapeCategory() {
        List<ShapeCategoryShapeManagementDTO> listShapeCategory = shapeCategoryService.getListShapeCategoryForShapeManagement();
        return ResponseEntity.ok().body(listShapeCategory);
    }
    
    @GetMapping({"/get-all-shape-management/{kidId}", "/get-all-shape-management/"})
    public ResponseEntity<List<ShapeManagementDTO>> getAllShapeManagement(@PathVariable(name = "kidId", required = false) Long kidId) {
    	
    	if(kidId == null) {
    		Authority kidAuthority = new Authority();
    		kidAuthority.setName(AuthoritiesConstants.USER);
    		
    		Optional<User> currentUser = Optional.empty();
    		Optional<String> username = SecurityUtils.getCurrentUserLogin();
    		
    		if(username.isPresent()) {
    			currentUser = userService.getUserWithAuthoritiesByLogin(username.get());
    		}
    		
    		if(currentUser.isPresent()) {
    			if(currentUser.get().getAuthorities().contains(kidAuthority)) {
    				kidId = currentUser.get().getId();
    			}
    		}
    	}
    	
        List<ShapeManagementDTO> listShapeManagementDTOs = shapeService.getAllShapeManagement(kidId);
        return ResponseEntity.ok().body(listShapeManagementDTOs);
    }
    
    @GetMapping("/get-shape-management/{id}")
    public ResponseEntity<ShapeManagementDTO> getShapeManagement(@PathVariable Long id) {
    	
        ShapeManagementDTO shapeManagementDTO = shapeService.getShapeManagement(id);
        return ResponseEntity.ok().body(shapeManagementDTO);
    }
    
    @PostMapping("/submit-shape-management")
    public ResponseEntity<ShapeManagementDTO> submitShapeManagement(@Valid @RequestBody ShapeVM shapeVM) {
    	
    	ShapeManagementDTO shapeManagementDTO = shapeService.submitShapeManagement(shapeVM);
        return ResponseEntity.ok().body(shapeManagementDTO);
    }
    
    @PostMapping("/save-shape-management")
    public ResponseEntity<ShapeManagementDTO> saveShapeManagement(@Valid @RequestBody ShapeVM shapeVM) {
    	
    	Authority kidAuthority = new Authority();
    	kidAuthority.setName(AuthoritiesConstants.USER);
    	
    	Optional<User> currentUser = Optional.empty();
    	Optional<String> username = SecurityUtils.getCurrentUserLogin();
    	
    	if(username.isPresent()) {
    		currentUser = userService.getUserWithAuthoritiesByLogin(username.get());
    	}
    	
    	if(currentUser.isPresent()) {
    		Long currentUserId = currentUser.get().getId();
    		String currentUserLogin = username.get();
    		
    		// set createBy value
    		shapeVM.setCreateById(currentUserId);
    		shapeVM.setCreateByLogin(currentUserLogin);
    		
    		// update updateBy value
    		shapeVM.setUpdateById(currentUserId);
    		shapeVM.setUpdateByLogin(currentUserLogin);
    		
    		// set kid value
    		if(shapeVM.getKidId() == null && currentUser.get().getAuthorities().contains(kidAuthority)) {
    			shapeVM.setKidId(currentUserId);
    			shapeVM.setKidLogin(currentUserLogin);
    		}
    	}
    	
    	ShapeManagementDTO shapeManagementDTO = shapeService.saveOrUpdateShapeManagement(shapeVM);
        return ResponseEntity.ok()
        		.headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "shape", shapeManagementDTO.getId().toString()))
        		.body(shapeManagementDTO);
    }
    
    @DeleteMapping("/delete-shape-management/{id}")
    public ResponseEntity<ShapeManagementDTO> deleteShapeManagement(@PathVariable Long id) {
    	
        ShapeManagementDTO shapeManagementDTO = shapeService.deleteShapeManagement(id);
        
        if(shapeManagementDTO == null) {
        	return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(shapeManagementDTO);
    }
}
