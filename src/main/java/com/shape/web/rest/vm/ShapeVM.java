package com.shape.web.rest.vm;

import java.util.List;

import com.shape.service.dto.ShapeDTO;
import com.shape.service.dto.ShapeRequirementDTO;

public class ShapeVM extends ShapeDTO {

	List<ShapeRequirementDTO> listShapeRequirement;

	public List<ShapeRequirementDTO> getListShapeRequirement() {
		return listShapeRequirement;
	}

	public void setListShapeRequirement(List<ShapeRequirementDTO> listShapeRequirement) {
		this.listShapeRequirement = listShapeRequirement;
	}

}
