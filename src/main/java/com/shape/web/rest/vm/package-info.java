/**
 * View Models used by Spring MVC REST controllers.
 */
package com.shape.web.rest.vm;
