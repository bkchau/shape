package com.shape.web.rest;

import com.shape.service.ShapeService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.ShapeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.Shape}.
 */
@RestController
@RequestMapping("/api")
public class ShapeResource {

    private final Logger log = LoggerFactory.getLogger(ShapeResource.class);

    private static final String ENTITY_NAME = "shape";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShapeService shapeService;

    public ShapeResource(ShapeService shapeService) {
        this.shapeService = shapeService;
    }

    /**
     * {@code POST  /shapes} : Create a new shape.
     *
     * @param shapeDTO the shapeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shapeDTO, or with status {@code 400 (Bad Request)} if the shape has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shapes")
    public ResponseEntity<ShapeDTO> createShape(@Valid @RequestBody ShapeDTO shapeDTO) throws URISyntaxException {
        log.debug("REST request to save Shape : {}", shapeDTO);
        if (shapeDTO.getId() != null) {
            throw new BadRequestAlertException("A new shape cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShapeDTO result = shapeService.save(shapeDTO);
        return ResponseEntity.created(new URI("/api/shapes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shapes} : Updates an existing shape.
     *
     * @param shapeDTO the shapeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shapeDTO,
     * or with status {@code 400 (Bad Request)} if the shapeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shapeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shapes")
    public ResponseEntity<ShapeDTO> updateShape(@Valid @RequestBody ShapeDTO shapeDTO) throws URISyntaxException {
        log.debug("REST request to update Shape : {}", shapeDTO);
        if (shapeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShapeDTO result = shapeService.save(shapeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shapeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shapes} : get all the shapes.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shapes in body.
     */
    @GetMapping("/shapes")
    public ResponseEntity<List<ShapeDTO>> getAllShapes(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Shapes");
        Page<ShapeDTO> page = shapeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shapes/:id} : get the "id" shape.
     *
     * @param id the id of the shapeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shapeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shapes/{id}")
    public ResponseEntity<ShapeDTO> getShape(@PathVariable Long id) {
        log.debug("REST request to get Shape : {}", id);
        Optional<ShapeDTO> shapeDTO = shapeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shapeDTO);
    }

    /**
     * {@code DELETE  /shapes/:id} : delete the "id" shape.
     *
     * @param id the id of the shapeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shapes/{id}")
    public ResponseEntity<Void> deleteShape(@PathVariable Long id) {
        log.debug("REST request to delete Shape : {}", id);
        shapeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
