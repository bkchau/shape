package com.shape.web.rest;

import com.shape.service.ShapeCategoryService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.ShapeCategoryDTO;
import com.shape.service.dto.ShapeCategoryShapeManagementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.ShapeCategory}.
 */
@RestController
@RequestMapping("/api")
public class ShapeCategoryResource {

    private final Logger log = LoggerFactory.getLogger(ShapeCategoryResource.class);

    private static final String ENTITY_NAME = "shapeCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShapeCategoryService shapeCategoryService;

    public ShapeCategoryResource(ShapeCategoryService shapeCategoryService) {
        this.shapeCategoryService = shapeCategoryService;
    }

    /**
     * {@code POST  /shape-categories} : Create a new shapeCategory.
     *
     * @param shapeCategoryDTO the shapeCategoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shapeCategoryDTO, or with status {@code 400 (Bad Request)} if the shapeCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shape-categories")
    public ResponseEntity<ShapeCategoryDTO> createShapeCategory(@Valid @RequestBody ShapeCategoryDTO shapeCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save ShapeCategory : {}", shapeCategoryDTO);
        if (shapeCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new shapeCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShapeCategoryDTO result = shapeCategoryService.save(shapeCategoryDTO);
        return ResponseEntity.created(new URI("/api/shape-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shape-categories} : Updates an existing shapeCategory.
     *
     * @param shapeCategoryDTO the shapeCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shapeCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the shapeCategoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shapeCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shape-categories")
    public ResponseEntity<ShapeCategoryDTO> updateShapeCategory(@Valid @RequestBody ShapeCategoryDTO shapeCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update ShapeCategory : {}", shapeCategoryDTO);
        if (shapeCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShapeCategoryDTO result = shapeCategoryService.save(shapeCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shapeCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shape-categories} : get all the shapeCategories.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shapeCategories in body.
     */
    @GetMapping("/shape-categories")
    public ResponseEntity<List<ShapeCategoryDTO>> getAllShapeCategories(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ShapeCategories");
        Page<ShapeCategoryDTO> page = shapeCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shape-categories/:id} : get the "id" shapeCategory.
     *
     * @param id the id of the shapeCategoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shapeCategoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shape-categories/{id}")
    public ResponseEntity<ShapeCategoryDTO> getShapeCategory(@PathVariable Long id) {
        log.debug("REST request to get ShapeCategory : {}", id);
        Optional<ShapeCategoryDTO> shapeCategoryDTO = shapeCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shapeCategoryDTO);
    }

    /**
     * {@code DELETE  /shape-categories/:id} : delete the "id" shapeCategory.
     *
     * @param id the id of the shapeCategoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shape-categories/{id}")
    public ResponseEntity<Void> deleteShapeCategory(@PathVariable Long id) {
        log.debug("REST request to delete ShapeCategory : {}", id);
        shapeCategoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
