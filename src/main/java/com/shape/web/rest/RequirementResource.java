package com.shape.web.rest;

import com.shape.service.RequirementService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.RequirementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.Requirement}.
 */
@RestController
@RequestMapping("/api")
public class RequirementResource {

    private final Logger log = LoggerFactory.getLogger(RequirementResource.class);

    private static final String ENTITY_NAME = "requirement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequirementService requirementService;

    public RequirementResource(RequirementService requirementService) {
        this.requirementService = requirementService;
    }

    /**
     * {@code POST  /requirements} : Create a new requirement.
     *
     * @param requirementDTO the requirementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new requirementDTO, or with status {@code 400 (Bad Request)} if the requirement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/requirements")
    public ResponseEntity<RequirementDTO> createRequirement(@Valid @RequestBody RequirementDTO requirementDTO) throws URISyntaxException {
        log.debug("REST request to save Requirement : {}", requirementDTO);
        if (requirementDTO.getId() != null) {
            throw new BadRequestAlertException("A new requirement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequirementDTO result = requirementService.save(requirementDTO);
        return ResponseEntity.created(new URI("/api/requirements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /requirements} : Updates an existing requirement.
     *
     * @param requirementDTO the requirementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requirementDTO,
     * or with status {@code 400 (Bad Request)} if the requirementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requirementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/requirements")
    public ResponseEntity<RequirementDTO> updateRequirement(@Valid @RequestBody RequirementDTO requirementDTO) throws URISyntaxException {
        log.debug("REST request to update Requirement : {}", requirementDTO);
        if (requirementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RequirementDTO result = requirementService.save(requirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requirementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /requirements} : get all the requirements.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requirements in body.
     */
    @GetMapping("/requirements")
    public ResponseEntity<List<RequirementDTO>> getAllRequirements(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Requirements");
        Page<RequirementDTO> page = requirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /requirements/:id} : get the "id" requirement.
     *
     * @param id the id of the requirementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requirementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/requirements/{id}")
    public ResponseEntity<RequirementDTO> getRequirement(@PathVariable Long id) {
        log.debug("REST request to get Requirement : {}", id);
        Optional<RequirementDTO> requirementDTO = requirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requirementDTO);
    }

    /**
     * {@code DELETE  /requirements/:id} : delete the "id" requirement.
     *
     * @param id the id of the requirementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/requirements/{id}")
    public ResponseEntity<Void> deleteRequirement(@PathVariable Long id) {
        log.debug("REST request to delete Requirement : {}", id);
        requirementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
