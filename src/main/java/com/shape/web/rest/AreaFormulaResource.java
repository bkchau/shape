package com.shape.web.rest;

import com.shape.service.AreaFormulaService;
import com.shape.web.rest.errors.BadRequestAlertException;
import com.shape.service.dto.AreaFormulaDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.shape.domain.AreaFormula}.
 */
@RestController
@RequestMapping("/api")
public class AreaFormulaResource {

    private final Logger log = LoggerFactory.getLogger(AreaFormulaResource.class);

    private static final String ENTITY_NAME = "areaFormula";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AreaFormulaService areaFormulaService;

    public AreaFormulaResource(AreaFormulaService areaFormulaService) {
        this.areaFormulaService = areaFormulaService;
    }

    /**
     * {@code POST  /area-formulas} : Create a new areaFormula.
     *
     * @param areaFormulaDTO the areaFormulaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new areaFormulaDTO, or with status {@code 400 (Bad Request)} if the areaFormula has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/area-formulas")
    public ResponseEntity<AreaFormulaDTO> createAreaFormula(@Valid @RequestBody AreaFormulaDTO areaFormulaDTO) throws URISyntaxException {
        log.debug("REST request to save AreaFormula : {}", areaFormulaDTO);
        if (areaFormulaDTO.getId() != null) {
            throw new BadRequestAlertException("A new areaFormula cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AreaFormulaDTO result = areaFormulaService.save(areaFormulaDTO);
        return ResponseEntity.created(new URI("/api/area-formulas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /area-formulas} : Updates an existing areaFormula.
     *
     * @param areaFormulaDTO the areaFormulaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated areaFormulaDTO,
     * or with status {@code 400 (Bad Request)} if the areaFormulaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the areaFormulaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/area-formulas")
    public ResponseEntity<AreaFormulaDTO> updateAreaFormula(@Valid @RequestBody AreaFormulaDTO areaFormulaDTO) throws URISyntaxException {
        log.debug("REST request to update AreaFormula : {}", areaFormulaDTO);
        if (areaFormulaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AreaFormulaDTO result = areaFormulaService.save(areaFormulaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, areaFormulaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /area-formulas} : get all the areaFormulas.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of areaFormulas in body.
     */
    @GetMapping("/area-formulas")
    public ResponseEntity<List<AreaFormulaDTO>> getAllAreaFormulas(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of AreaFormulas");
        Page<AreaFormulaDTO> page = areaFormulaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /area-formulas/:id} : get the "id" areaFormula.
     *
     * @param id the id of the areaFormulaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the areaFormulaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/area-formulas/{id}")
    public ResponseEntity<AreaFormulaDTO> getAreaFormula(@PathVariable Long id) {
        log.debug("REST request to get AreaFormula : {}", id);
        Optional<AreaFormulaDTO> areaFormulaDTO = areaFormulaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(areaFormulaDTO);
    }

    /**
     * {@code DELETE  /area-formulas/:id} : delete the "id" areaFormula.
     *
     * @param id the id of the areaFormulaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/area-formulas/{id}")
    public ResponseEntity<Void> deleteAreaFormula(@PathVariable Long id) {
        log.debug("REST request to delete AreaFormula : {}", id);
        areaFormulaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
