package com.shape.repository;

import com.shape.domain.ShapeCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShapeCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShapeCategoryRepository extends JpaRepository<ShapeCategory, Long> {

}
