package com.shape.repository;

import com.shape.domain.RequirementSet;
import com.shape.domain.ShapeCategory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the RequirementSet entity.
 */
@Repository
public interface RequirementSetRepository extends JpaRepository<RequirementSet, Long> {

    @Query(value = "select distinct requirementSet from RequirementSet requirementSet left join fetch requirementSet.requirements",
        countQuery = "select count(distinct requirementSet) from RequirementSet requirementSet")
    Page<RequirementSet> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct requirementSet from RequirementSet requirementSet left join fetch requirementSet.requirements")
    List<RequirementSet> findAllWithEagerRelationships();

    @Query("select requirementSet from RequirementSet requirementSet left join fetch requirementSet.requirements where requirementSet.id =:id")
    Optional<RequirementSet> findOneWithEagerRelationships(@Param("id") Long id);

    List<RequirementSet> findAllByShapeCategory(ShapeCategory shapeCategory);
}
