package com.shape.repository;

import com.shape.domain.Shape;
import com.shape.domain.ShapeRequirement;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShapeRequirement entity.
 */
@Repository
public interface ShapeRequirementRepository extends JpaRepository<ShapeRequirement, Long> {
	
	List<ShapeRequirement> findAllByShape(Shape shape);
}
