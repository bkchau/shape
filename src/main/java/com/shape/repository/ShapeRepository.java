package com.shape.repository;

import com.shape.domain.Shape;
import com.shape.domain.User;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Shape entity.
 */
@Repository
public interface ShapeRepository extends JpaRepository<Shape, Long> {

    @Query("select shape from Shape shape where shape.kid.login = ?#{principal.username}")
    List<Shape> findByKidIsCurrentUser();

    @Query("select shape from Shape shape where shape.createBy.login = ?#{principal.username}")
    List<Shape> findByCreateByIsCurrentUser();

    @Query("select shape from Shape shape where shape.updateBy.login = ?#{principal.username}")
    List<Shape> findByUpdateByIsCurrentUser();

    Shape findOneById(Long id);
    
    List<Shape> findAllByIsDeleted(Boolean isDeleted);
    
    List<Shape> findAllByKidAndIsDeleted(User kid, Boolean isDeleted);
}
