package com.shape.repository;

import com.shape.domain.AreaFormula;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AreaFormula entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AreaFormulaRepository extends JpaRepository<AreaFormula, Long> {

}
