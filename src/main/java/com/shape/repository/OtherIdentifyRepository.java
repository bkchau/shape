package com.shape.repository;

import com.shape.domain.OtherIdentify;
import com.shape.domain.RequirementSet;
import com.shape.domain.ShapeCategory;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OtherIdentify entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherIdentifyRepository extends JpaRepository<OtherIdentify, Long> {

	List<OtherIdentify> findAllByShapeCategory(ShapeCategory shapeCategory);
}
