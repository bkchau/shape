package com.shape.service;

import com.shape.service.dto.RequirementSetDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.RequirementSet}.
 */
public interface RequirementSetService {

    /**
     * Save a requirementSet.
     *
     * @param requirementSetDTO the entity to save.
     * @return the persisted entity.
     */
    RequirementSetDTO save(RequirementSetDTO requirementSetDTO);

    /**
     * Get all the requirementSets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RequirementSetDTO> findAll(Pageable pageable);

    /**
     * Get all the requirementSets with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<RequirementSetDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" requirementSet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequirementSetDTO> findOne(Long id);

    /**
     * Delete the "id" requirementSet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
