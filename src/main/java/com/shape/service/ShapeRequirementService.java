package com.shape.service;

import com.shape.service.dto.ShapeRequirementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.ShapeRequirement}.
 */
public interface ShapeRequirementService {

    /**
     * Save a shapeRequirement.
     *
     * @param shapeRequirementDTO the entity to save.
     * @return the persisted entity.
     */
    ShapeRequirementDTO save(ShapeRequirementDTO shapeRequirementDTO);

    /**
     * Get all the shapeRequirements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShapeRequirementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shapeRequirement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShapeRequirementDTO> findOne(Long id);

    /**
     * Delete the "id" shapeRequirement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
