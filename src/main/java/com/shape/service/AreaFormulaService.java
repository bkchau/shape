package com.shape.service;

import com.shape.service.dto.AreaFormulaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.AreaFormula}.
 */
public interface AreaFormulaService {

    /**
     * Save a areaFormula.
     *
     * @param areaFormulaDTO the entity to save.
     * @return the persisted entity.
     */
    AreaFormulaDTO save(AreaFormulaDTO areaFormulaDTO);

    /**
     * Get all the areaFormulas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AreaFormulaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" areaFormula.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AreaFormulaDTO> findOne(Long id);

    /**
     * Delete the "id" areaFormula.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
