package com.shape.service.impl;

import com.shape.service.ShapeService;
import com.shape.domain.OtherIdentify;
import com.shape.domain.Shape;
import com.shape.domain.ShapeCategory;
import com.shape.domain.ShapeRequirement;
import com.shape.repository.OtherIdentifyRepository;
import com.shape.repository.RequirementSetRepository;
import com.shape.repository.ShapeCategoryRepository;
import com.shape.repository.ShapeRepository;
import com.shape.repository.ShapeRequirementRepository;
import com.shape.service.dto.OtherIdentifyDTO;
import com.shape.service.dto.ShapeDTO;
import com.shape.service.dto.ShapeManagementDTO;
import com.shape.service.dto.ShapeRequirementDTO;
import com.shape.service.mapper.OtherIdentifyMapper;
import com.shape.service.mapper.RequirementSetMapper;
import com.shape.service.mapper.ShapeCategoryMapper;
import com.shape.service.mapper.ShapeMapper;
import com.shape.service.mapper.ShapeRequirementMapper;
import com.shape.service.mapper.UserMapper;
import com.shape.web.rest.vm.ShapeVM;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Shape}.
 */
@Service
@Transactional
public class ShapeServiceImpl implements ShapeService {

    private final Logger log = LoggerFactory.getLogger(ShapeServiceImpl.class);

    private final ShapeRepository shapeRepository;

    private final ShapeMapper shapeMapper;
    
    private final OtherIdentifyRepository otherIdentifyRepository;
    
    private final OtherIdentifyMapper otherIdentifyMapper;
    
    private final ShapeCategoryRepository shapeCategoryRepository;
    
    private final ShapeCategoryMapper shapeCategoryMapper;
    
    private final RequirementSetRepository requirementSetRepository;
    
    private final RequirementSetMapper requirementSetMapper;
    
    private final ShapeRequirementRepository shapeRequirementRepository;
    
    private final ShapeRequirementMapper shapeRequirementMapper;
    
    private final UserMapper userMapper;

    public ShapeServiceImpl(ShapeRepository shapeRepository, ShapeMapper shapeMapper,
    		OtherIdentifyRepository otherIdentifyRepository, OtherIdentifyMapper otherIdentifyMapper,
    		ShapeCategoryRepository shapeCategoryRepository, ShapeCategoryMapper shapeCategoryMapper,
    		RequirementSetRepository requirementSetRepository, RequirementSetMapper requirementSetMapper,
    		ShapeRequirementRepository shapeRequirementRepository, ShapeRequirementMapper shapeRequirementMapper,
    		UserMapper userMapper) {
        this.shapeRepository = shapeRepository;
        this.shapeMapper = shapeMapper;
        this.otherIdentifyRepository = otherIdentifyRepository;
        this.otherIdentifyMapper = otherIdentifyMapper;
        this.shapeCategoryRepository = shapeCategoryRepository;
        this.shapeCategoryMapper = shapeCategoryMapper;
        this.requirementSetRepository = requirementSetRepository;
        this.requirementSetMapper = requirementSetMapper;
        this.shapeRequirementRepository = shapeRequirementRepository;
        this.shapeRequirementMapper = shapeRequirementMapper;
        this.userMapper = userMapper;
    }

    /**
     * Save a shape.
     *
     * @param shapeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShapeDTO save(ShapeDTO shapeDTO) {
        log.debug("Request to save Shape : {}", shapeDTO);
        Shape shape = shapeMapper.toEntity(shapeDTO);
        shape = shapeRepository.save(shape);
        return shapeMapper.toDto(shape);
    }

    /**
     * Get all the shapes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShapeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shapes");
        return shapeRepository.findAll(pageable)
            .map(shapeMapper::toDto);
    }


    /**
     * Get one shape by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShapeDTO> findOne(Long id) {
        log.debug("Request to get Shape : {}", id);
        return shapeRepository.findById(id)
            .map(shapeMapper::toDto);
    }

    /**
     * Delete the shape by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Shape : {}", id);
        shapeRepository.deleteById(id);
    }
    
	@Override
	public List<ShapeManagementDTO> getAllShapeManagement(Long kidId) {
		List<ShapeManagementDTO> listShapeManagementDTOs = null;
		List<Shape> listShape = null;

		if (kidId == null) {
			listShape = shapeRepository.findAllByIsDeleted(null);
		}
		else {
			listShape = shapeRepository.findAllByKidAndIsDeleted(userMapper.userFromId(kidId), null);
		}

		if (listShape != null && listShape.size() > 0) {
			listShapeManagementDTOs = shapeMapper.toShapeManagementDTOs(listShape);

			listShapeManagementDTOs = listShapeManagementDTOs.stream()
					.map((dto) -> {
						List<ShapeRequirement> listShapeRequirements = shapeRequirementRepository.findAllByShape(shapeMapper.fromId(dto.getId()));
						List<ShapeRequirementDTO> listShapeRequirementDTOs = shapeRequirementMapper.toDto(listShapeRequirements);
		
						dto.setShapeRequirements(listShapeRequirementDTOs);
		
						return dto;
					})
					.collect(Collectors.toList());
		}

		return listShapeManagementDTOs;
	}
	
	@Override
	public ShapeManagementDTO getShapeManagement(Long id) {
		Shape shape = shapeRepository.findOneById(id);
		
		List<ShapeRequirement> listShapeRequirements = shapeRequirementRepository.findAllByShape(shape);
		List<ShapeRequirementDTO> listShapeRequirementDTOs = shapeRequirementMapper.toDto(listShapeRequirements);
		
		ShapeManagementDTO shapeManagementDTO = shapeMapper.toShapeManagementDTO(shape);
		shapeManagementDTO.setShapeRequirements(listShapeRequirementDTOs);
		
		return shapeManagementDTO;
	}

    @Override
    public ShapeManagementDTO submitShapeManagement(ShapeVM shapeVM) {
    	ShapeManagementDTO shapeManagementDTO = new ShapeManagementDTO();
    	
    	// get list possible category
    	ShapeCategory shapeCategory = shapeCategoryMapper.fromId(shapeVM.getShapeCategoryId());
    	List<OtherIdentify> listOtherIdentifies = otherIdentifyRepository.findAllByShapeCategory(shapeCategory);
    	List<OtherIdentifyDTO> listOtherIdentifyDTOs = otherIdentifyMapper.toDto(listOtherIdentifies);
    	shapeManagementDTO.setOtherIdenfifies(listOtherIdentifyDTOs);
    	
    	// calculate area
    	// TODO: call shapeCategoryRepository to get this value
    	
    	
    	return shapeManagementDTO;
    }
    
    @Override
    public ShapeManagementDTO saveOrUpdateShapeManagement(ShapeVM shapeVM) {
    	ShapeManagementDTO shapeManagementDTO = new ShapeManagementDTO();
    	
    	Shape shape = new Shape();
    	
    	if(shapeVM.getId() != null) {
    		shape = shapeRepository.findOneById(shapeVM.getId());
    	}
    	else {
    		shape.setCreateBy(userMapper.userFromId(shapeVM.getCreateById()));
    		shape.setCreateAt(LocalDate.now());
    		shape.setIsDeleted(null);
    	}
    	
    	shape.setName(shapeVM.getName());
    	shape.setArea(shapeVM.getArea());
    	shape.setShapeCategory(shapeCategoryMapper.fromId(shapeVM.getShapeCategoryId()));
    	shape.setRequirementSet(requirementSetMapper.fromId(shapeVM.getRequirementSetId()));
    	shape.setKid(userMapper.userFromId(shapeVM.getKidId()));
    	shape.setUpdateBy(userMapper.userFromId(shapeVM.getUpdateById()));
    	shape.setUpdateAt(LocalDate.now());
    	shape.setIsDeleted(shapeVM.isIsDeleted());
    	
    	// store shape
    	shape = shapeRepository.save(shape);
    	
    	shapeManagementDTO = shapeMapper.toShapeManagementDTO(shape);
    	
    	final Long shapeId = shape.getId();
    	List<ShapeRequirementDTO> listShapeRequirementDTOs = shapeVM.getListShapeRequirement();
    	
    	// set shapeId
    	listShapeRequirementDTOs.forEach(dto -> {dto.setShapeId(shapeId);});
    	
    	// convert to entity
    	List<ShapeRequirement> listShapeRequirements = shapeRequirementMapper.toEntity(listShapeRequirementDTOs);
    	
    	// store list ShapeRequirement to db
    	listShapeRequirements = shapeRequirementRepository.saveAll(listShapeRequirements);
    	
    	// convert to DTO
    	listShapeRequirementDTOs = shapeRequirementMapper.toDto(listShapeRequirements);
    	
    	shapeManagementDTO.setShapeRequirements(listShapeRequirementDTOs);
    	
    	// get list possible category
    	ShapeCategory shapeCategory = shapeCategoryMapper.fromId(shapeVM.getShapeCategoryId());
    	List<OtherIdentify> listOtherIdentifies = otherIdentifyRepository.findAllByShapeCategory(shapeCategory);
    	List<OtherIdentifyDTO> listOtherIdentifyDTOs = otherIdentifyMapper.toDto(listOtherIdentifies);
    	shapeManagementDTO.setOtherIdenfifies(listOtherIdentifyDTOs); 	
    	
    	return shapeManagementDTO;
    }
    
    @Override
    public ShapeManagementDTO deleteShapeManagement(Long id) {
    	ShapeManagementDTO shapeManagementDTO = null;
    	
    	Shape shape = shapeRepository.findOneById(id);
    	
    	if(shape != null) {
    		shape.setIsDeleted(true);
    		
    		shape = shapeRepository.save(shape);
    		
    		shapeManagementDTO = shapeMapper.toShapeManagementDTO(shape);
    	}
    	
    	return shapeManagementDTO;
    }
}
