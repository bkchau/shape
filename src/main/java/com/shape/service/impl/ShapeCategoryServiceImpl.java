package com.shape.service.impl;

import com.shape.service.ShapeCategoryService;
import com.shape.domain.RequirementSet;
import com.shape.domain.ShapeCategory;
import com.shape.repository.RequirementSetRepository;
import com.shape.repository.ShapeCategoryRepository;
import com.shape.service.dto.RequirementSetDTO;
import com.shape.service.dto.ShapeCategoryDTO;
import com.shape.service.dto.ShapeCategoryShapeManagementDTO;
import com.shape.service.mapper.RequirementSetMapper;
import com.shape.service.mapper.ShapeCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ShapeCategory}.
 */
@Service
@Transactional
public class ShapeCategoryServiceImpl implements ShapeCategoryService {

    private final Logger log = LoggerFactory.getLogger(ShapeCategoryServiceImpl.class);

    private final ShapeCategoryRepository shapeCategoryRepository;

    private final ShapeCategoryMapper shapeCategoryMapper;
    
    private final RequirementSetRepository requirementSetRepository;
    
    private final RequirementSetMapper requirementSetMapper;

    public ShapeCategoryServiceImpl(ShapeCategoryRepository shapeCategoryRepository, ShapeCategoryMapper shapeCategoryMapper,
    		RequirementSetRepository requirementSetRepository, RequirementSetMapper requirementSetMapper) {
        this.shapeCategoryRepository = shapeCategoryRepository;
        this.shapeCategoryMapper = shapeCategoryMapper;
        this.requirementSetRepository = requirementSetRepository;
        this.requirementSetMapper = requirementSetMapper;
    }

    /**
     * Save a shapeCategory.
     *
     * @param shapeCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShapeCategoryDTO save(ShapeCategoryDTO shapeCategoryDTO) {
        log.debug("Request to save ShapeCategory : {}", shapeCategoryDTO);
        ShapeCategory shapeCategory = shapeCategoryMapper.toEntity(shapeCategoryDTO);
        shapeCategory = shapeCategoryRepository.save(shapeCategory);
        return shapeCategoryMapper.toDto(shapeCategory);
    }

    /**
     * Get all the shapeCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShapeCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShapeCategories");
        return shapeCategoryRepository.findAll(pageable)
            .map(shapeCategoryMapper::toDto);
    }


    /**
     * Get one shapeCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShapeCategoryDTO> findOne(Long id) {
        log.debug("Request to get ShapeCategory : {}", id);
        return shapeCategoryRepository.findById(id)
            .map(shapeCategoryMapper::toDto);
    }

    /**
     * Delete the shapeCategory by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShapeCategory : {}", id);
        shapeCategoryRepository.deleteById(id);
    }
    
    @Override
    public List<ShapeCategoryShapeManagementDTO> getListShapeCategoryForShapeManagement(){
    	
    	// TODO: customize this function to get list by one db connection
    	List<ShapeCategory> listShapeCategory = shapeCategoryRepository.findAll();
    	
    	List<ShapeCategoryShapeManagementDTO> result = listShapeCategory
    			.stream()
    			.map((shapeCategory)->{    				
    				List<RequirementSet> listRequirementSet = requirementSetRepository.findAllByShapeCategory(shapeCategory);
    				List<RequirementSetDTO> listRequirementSetDTO = requirementSetMapper.toDto(listRequirementSet);
    				
    				ShapeCategoryShapeManagementDTO smDTO = new ShapeCategoryShapeManagementDTO();
    				smDTO.setId(shapeCategory.getId());
    				smDTO.setName(shapeCategory.getName());
    				smDTO.setRequirementSet(listRequirementSetDTO);
    				
    				return smDTO;
		    	})
    			.collect(Collectors.toList());
    	
    	return result;
    }
}
