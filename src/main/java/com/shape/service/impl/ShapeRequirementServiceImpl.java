package com.shape.service.impl;

import com.shape.service.ShapeRequirementService;
import com.shape.domain.ShapeRequirement;
import com.shape.repository.ShapeRequirementRepository;
import com.shape.service.dto.ShapeRequirementDTO;
import com.shape.service.mapper.ShapeRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShapeRequirement}.
 */
@Service
@Transactional
public class ShapeRequirementServiceImpl implements ShapeRequirementService {

    private final Logger log = LoggerFactory.getLogger(ShapeRequirementServiceImpl.class);

    private final ShapeRequirementRepository shapeRequirementRepository;

    private final ShapeRequirementMapper shapeRequirementMapper;

    public ShapeRequirementServiceImpl(ShapeRequirementRepository shapeRequirementRepository, ShapeRequirementMapper shapeRequirementMapper) {
        this.shapeRequirementRepository = shapeRequirementRepository;
        this.shapeRequirementMapper = shapeRequirementMapper;
    }

    /**
     * Save a shapeRequirement.
     *
     * @param shapeRequirementDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShapeRequirementDTO save(ShapeRequirementDTO shapeRequirementDTO) {
        log.debug("Request to save ShapeRequirement : {}", shapeRequirementDTO);
        ShapeRequirement shapeRequirement = shapeRequirementMapper.toEntity(shapeRequirementDTO);
        shapeRequirement = shapeRequirementRepository.save(shapeRequirement);
        return shapeRequirementMapper.toDto(shapeRequirement);
    }

    /**
     * Get all the shapeRequirements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShapeRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShapeRequirements");
        return shapeRequirementRepository.findAll(pageable)
            .map(shapeRequirementMapper::toDto);
    }


    /**
     * Get one shapeRequirement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShapeRequirementDTO> findOne(Long id) {
        log.debug("Request to get ShapeRequirement : {}", id);
        return shapeRequirementRepository.findById(id)
            .map(shapeRequirementMapper::toDto);
    }

    /**
     * Delete the shapeRequirement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShapeRequirement : {}", id);
        shapeRequirementRepository.deleteById(id);
    }
}
