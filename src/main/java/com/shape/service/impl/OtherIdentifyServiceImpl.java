package com.shape.service.impl;

import com.shape.service.OtherIdentifyService;
import com.shape.domain.OtherIdentify;
import com.shape.repository.OtherIdentifyRepository;
import com.shape.service.dto.OtherIdentifyDTO;
import com.shape.service.mapper.OtherIdentifyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OtherIdentify}.
 */
@Service
@Transactional
public class OtherIdentifyServiceImpl implements OtherIdentifyService {

    private final Logger log = LoggerFactory.getLogger(OtherIdentifyServiceImpl.class);

    private final OtherIdentifyRepository otherIdentifyRepository;

    private final OtherIdentifyMapper otherIdentifyMapper;

    public OtherIdentifyServiceImpl(OtherIdentifyRepository otherIdentifyRepository, OtherIdentifyMapper otherIdentifyMapper) {
        this.otherIdentifyRepository = otherIdentifyRepository;
        this.otherIdentifyMapper = otherIdentifyMapper;
    }

    /**
     * Save a otherIdentify.
     *
     * @param otherIdentifyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OtherIdentifyDTO save(OtherIdentifyDTO otherIdentifyDTO) {
        log.debug("Request to save OtherIdentify : {}", otherIdentifyDTO);
        OtherIdentify otherIdentify = otherIdentifyMapper.toEntity(otherIdentifyDTO);
        otherIdentify = otherIdentifyRepository.save(otherIdentify);
        return otherIdentifyMapper.toDto(otherIdentify);
    }

    /**
     * Get all the otherIdentifies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OtherIdentifyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OtherIdentifies");
        return otherIdentifyRepository.findAll(pageable)
            .map(otherIdentifyMapper::toDto);
    }


    /**
     * Get one otherIdentify by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OtherIdentifyDTO> findOne(Long id) {
        log.debug("Request to get OtherIdentify : {}", id);
        return otherIdentifyRepository.findById(id)
            .map(otherIdentifyMapper::toDto);
    }

    /**
     * Delete the otherIdentify by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OtherIdentify : {}", id);
        otherIdentifyRepository.deleteById(id);
    }
}
