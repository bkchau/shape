package com.shape.service.impl;

import com.shape.service.RequirementSetService;
import com.shape.domain.RequirementSet;
import com.shape.repository.RequirementSetRepository;
import com.shape.service.dto.RequirementSetDTO;
import com.shape.service.mapper.RequirementSetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RequirementSet}.
 */
@Service
@Transactional
public class RequirementSetServiceImpl implements RequirementSetService {

    private final Logger log = LoggerFactory.getLogger(RequirementSetServiceImpl.class);

    private final RequirementSetRepository requirementSetRepository;

    private final RequirementSetMapper requirementSetMapper;

    public RequirementSetServiceImpl(RequirementSetRepository requirementSetRepository, RequirementSetMapper requirementSetMapper) {
        this.requirementSetRepository = requirementSetRepository;
        this.requirementSetMapper = requirementSetMapper;
    }

    /**
     * Save a requirementSet.
     *
     * @param requirementSetDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RequirementSetDTO save(RequirementSetDTO requirementSetDTO) {
        log.debug("Request to save RequirementSet : {}", requirementSetDTO);
        RequirementSet requirementSet = requirementSetMapper.toEntity(requirementSetDTO);
        requirementSet = requirementSetRepository.save(requirementSet);
        return requirementSetMapper.toDto(requirementSet);
    }

    /**
     * Get all the requirementSets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RequirementSetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequirementSets");
        return requirementSetRepository.findAll(pageable)
            .map(requirementSetMapper::toDto);
    }

    /**
     * Get all the requirementSets with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<RequirementSetDTO> findAllWithEagerRelationships(Pageable pageable) {
        return requirementSetRepository.findAllWithEagerRelationships(pageable).map(requirementSetMapper::toDto);
    }
    

    /**
     * Get one requirementSet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RequirementSetDTO> findOne(Long id) {
        log.debug("Request to get RequirementSet : {}", id);
        return requirementSetRepository.findOneWithEagerRelationships(id)
            .map(requirementSetMapper::toDto);
    }

    /**
     * Delete the requirementSet by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RequirementSet : {}", id);
        requirementSetRepository.deleteById(id);
    }
}
