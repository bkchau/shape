package com.shape.service.impl;

import com.shape.service.AreaFormulaService;
import com.shape.domain.AreaFormula;
import com.shape.repository.AreaFormulaRepository;
import com.shape.service.dto.AreaFormulaDTO;
import com.shape.service.mapper.AreaFormulaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AreaFormula}.
 */
@Service
@Transactional
public class AreaFormulaServiceImpl implements AreaFormulaService {

    private final Logger log = LoggerFactory.getLogger(AreaFormulaServiceImpl.class);

    private final AreaFormulaRepository areaFormulaRepository;

    private final AreaFormulaMapper areaFormulaMapper;

    public AreaFormulaServiceImpl(AreaFormulaRepository areaFormulaRepository, AreaFormulaMapper areaFormulaMapper) {
        this.areaFormulaRepository = areaFormulaRepository;
        this.areaFormulaMapper = areaFormulaMapper;
    }

    /**
     * Save a areaFormula.
     *
     * @param areaFormulaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AreaFormulaDTO save(AreaFormulaDTO areaFormulaDTO) {
        log.debug("Request to save AreaFormula : {}", areaFormulaDTO);
        AreaFormula areaFormula = areaFormulaMapper.toEntity(areaFormulaDTO);
        areaFormula = areaFormulaRepository.save(areaFormula);
        return areaFormulaMapper.toDto(areaFormula);
    }

    /**
     * Get all the areaFormulas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AreaFormulaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AreaFormulas");
        return areaFormulaRepository.findAll(pageable)
            .map(areaFormulaMapper::toDto);
    }


    /**
     * Get one areaFormula by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AreaFormulaDTO> findOne(Long id) {
        log.debug("Request to get AreaFormula : {}", id);
        return areaFormulaRepository.findById(id)
            .map(areaFormulaMapper::toDto);
    }

    /**
     * Delete the areaFormula by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AreaFormula : {}", id);
        areaFormulaRepository.deleteById(id);
    }
}
