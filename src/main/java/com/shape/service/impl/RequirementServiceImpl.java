package com.shape.service.impl;

import com.shape.service.RequirementService;
import com.shape.domain.Requirement;
import com.shape.repository.RequirementRepository;
import com.shape.service.dto.RequirementDTO;
import com.shape.service.mapper.RequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Requirement}.
 */
@Service
@Transactional
public class RequirementServiceImpl implements RequirementService {

    private final Logger log = LoggerFactory.getLogger(RequirementServiceImpl.class);

    private final RequirementRepository requirementRepository;

    private final RequirementMapper requirementMapper;

    public RequirementServiceImpl(RequirementRepository requirementRepository, RequirementMapper requirementMapper) {
        this.requirementRepository = requirementRepository;
        this.requirementMapper = requirementMapper;
    }

    /**
     * Save a requirement.
     *
     * @param requirementDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RequirementDTO save(RequirementDTO requirementDTO) {
        log.debug("Request to save Requirement : {}", requirementDTO);
        Requirement requirement = requirementMapper.toEntity(requirementDTO);
        requirement = requirementRepository.save(requirement);
        return requirementMapper.toDto(requirement);
    }

    /**
     * Get all the requirements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Requirements");
        return requirementRepository.findAll(pageable)
            .map(requirementMapper::toDto);
    }


    /**
     * Get one requirement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RequirementDTO> findOne(Long id) {
        log.debug("Request to get Requirement : {}", id);
        return requirementRepository.findById(id)
            .map(requirementMapper::toDto);
    }

    /**
     * Delete the requirement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Requirement : {}", id);
        requirementRepository.deleteById(id);
    }
}
