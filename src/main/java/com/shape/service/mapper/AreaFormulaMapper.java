package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.AreaFormulaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AreaFormula} and its DTO {@link AreaFormulaDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequirementSetMapper.class})
public interface AreaFormulaMapper extends EntityMapper<AreaFormulaDTO, AreaFormula> {

    @Mapping(source = "requirementSet.id", target = "requirementSetId")
    @Mapping(source = "requirementSet.name", target = "requirementSetName")
    AreaFormulaDTO toDto(AreaFormula areaFormula);

    @Mapping(source = "requirementSetId", target = "requirementSet")
    AreaFormula toEntity(AreaFormulaDTO areaFormulaDTO);

    default AreaFormula fromId(Long id) {
        if (id == null) {
            return null;
        }
        AreaFormula areaFormula = new AreaFormula();
        areaFormula.setId(id);
        return areaFormula;
    }
}
