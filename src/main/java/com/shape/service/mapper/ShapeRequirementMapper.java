package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.ShapeRequirementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShapeRequirement} and its DTO {@link ShapeRequirementDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShapeMapper.class, RequirementMapper.class})
public interface ShapeRequirementMapper extends EntityMapper<ShapeRequirementDTO, ShapeRequirement> {

    @Mapping(source = "shape.id", target = "shapeId")
    @Mapping(source = "shape.name", target = "shapeName")
    @Mapping(source = "requirement.id", target = "requirementId")
    @Mapping(source = "requirement.name", target = "requirementName")
    ShapeRequirementDTO toDto(ShapeRequirement shapeRequirement);

    @Mapping(source = "shapeId", target = "shape")
    @Mapping(source = "requirementId", target = "requirement")
    ShapeRequirement toEntity(ShapeRequirementDTO shapeRequirementDTO);

    default ShapeRequirement fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShapeRequirement shapeRequirement = new ShapeRequirement();
        shapeRequirement.setId(id);
        return shapeRequirement;
    }
}
