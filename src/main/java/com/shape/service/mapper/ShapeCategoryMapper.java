package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.ShapeCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShapeCategory} and its DTO {@link ShapeCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShapeCategoryMapper extends EntityMapper<ShapeCategoryDTO, ShapeCategory> {



    default ShapeCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShapeCategory shapeCategory = new ShapeCategory();
        shapeCategory.setId(id);
        return shapeCategory;
    }
}
