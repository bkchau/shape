package com.shape.service.mapper;

import java.util.List;

import com.shape.domain.Shape;
import com.shape.service.dto.ShapeDTO;
import com.shape.service.dto.ShapeManagementDTO;

/**
 * Mapper for the entity {@link Shape} and its DTO {@link ShapeDTO}.
 */
public interface ShapeMapper extends EntityMapper<ShapeDTO, Shape> {

    ShapeDTO toDto(Shape shape);

    Shape toEntity(ShapeDTO shapeDTO);
    
    ShapeManagementDTO toShapeManagementDTO(Shape shape);
    
    List<ShapeManagementDTO> toShapeManagementDTOs(List<Shape> shapes);

    default Shape fromId(Long id) {
        if (id == null) {
            return null;
        }
        Shape shape = new Shape();
        shape.setId(id);
        return shape;
    }
}
