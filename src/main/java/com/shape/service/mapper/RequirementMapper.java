package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.RequirementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Requirement} and its DTO {@link RequirementDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RequirementMapper extends EntityMapper<RequirementDTO, Requirement> {


    @Mapping(target = "requirementSets", ignore = true)
    @Mapping(target = "removeRequirementSets", ignore = true)
    Requirement toEntity(RequirementDTO requirementDTO);

    default Requirement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Requirement requirement = new Requirement();
        requirement.setId(id);
        return requirement;
    }
}
