package com.shape.service.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shape.domain.RequirementSet;
import com.shape.domain.Shape;
import com.shape.domain.ShapeCategory;
import com.shape.domain.User;
import com.shape.service.dto.ShapeDTO;
import com.shape.service.dto.ShapeManagementDTO;

@Component
public class ShapeMapperImpl implements ShapeMapper {

    @Autowired
    private ShapeCategoryMapper shapeCategoryMapper;
    @Autowired
    private RequirementSetMapper requirementSetMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Shape> toEntity(List<ShapeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Shape> list = new ArrayList<Shape>( dtoList.size() );
        for ( ShapeDTO shapeDTO : dtoList ) {
            list.add( toEntity( shapeDTO ) );
        }

        return list;
    }

    @Override
    public List<ShapeDTO> toDto(List<Shape> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ShapeDTO> list = new ArrayList<ShapeDTO>( entityList.size() );
        for ( Shape shape : entityList ) {
            list.add( toDto( shape ) );
        }

        return list;
    }

    @Override
    public ShapeDTO toDto(Shape shape) {
        if ( shape == null ) {
            return null;
        }

        ShapeDTO shapeDTO = new ShapeDTO();

        shapeDTO.setKidLogin( shapeKidLogin( shape ) );
        shapeDTO.setCreateByLogin( shapeCreateByLogin( shape ) );
        shapeDTO.setShapeCategoryName( shapeShapeCategoryName( shape ) );
        shapeDTO.setKidId( shapeKidId( shape ) );
        shapeDTO.setShapeCategoryId( shapeShapeCategoryId( shape ) );
        shapeDTO.setCreateById( shapeCreateById( shape ) );
        shapeDTO.setUpdateByLogin( shapeUpdateByLogin( shape ) );
        shapeDTO.setUpdateById( shapeUpdateById( shape ) );
        shapeDTO.setId( shape.getId() );
        shapeDTO.setName( shape.getName() );
        shapeDTO.setArea( shape.getArea() );
        shapeDTO.setCreateAt( shape.getCreateAt() );
        shapeDTO.setUpdateAt( shape.getUpdateAt() );
        shapeDTO.setIsDeleted( shape.isIsDeleted() );
        shapeDTO.setRequirementSetId( shapeRequirementSetId( shape ) );
        shapeDTO.setRequirementSetName( shapeRequirementSetName( shape ) );

        return shapeDTO;
    }

    @Override
    public Shape toEntity(ShapeDTO shapeDTO) {
        if ( shapeDTO == null ) {
            return null;
        }

        Shape shape = new Shape();

        shape.setShapeCategory( shapeCategoryMapper.fromId( shapeDTO.getShapeCategoryId() ) );
        shape.setRequirementSet( requirementSetMapper.fromId( shapeDTO.getRequirementSetId() ) );
        shape.setCreateBy( userMapper.userFromId( shapeDTO.getCreateById() ) );
        shape.setUpdateBy( userMapper.userFromId( shapeDTO.getUpdateById() ) );
        shape.setKid( userMapper.userFromId( shapeDTO.getKidId() ) );
        shape.setId( shapeDTO.getId() );
        shape.setName( shapeDTO.getName() );
        shape.setArea( shapeDTO.getArea() );
        shape.setCreateAt( shapeDTO.getCreateAt() );
        shape.setUpdateAt( shapeDTO.getUpdateAt() );
        shape.setIsDeleted( shapeDTO.isIsDeleted() );

        return shape;
    }

	public ShapeManagementDTO toShapeManagementDTO(Shape shape) {
		if (shape == null) {
			return null;
		}

		ShapeManagementDTO shapeManagementDTO = new ShapeManagementDTO();

		shapeManagementDTO.setId(shape.getId());
		shapeManagementDTO.setName(shape.getName());
		shapeManagementDTO.setArea(shape.getArea());

		if (shape.getShapeCategory() != null) {
			shapeManagementDTO.setShapeCategoryId(shape.getShapeCategory().getId());
			shapeManagementDTO.setShapeCategoryName(shape.getShapeCategory().getName());
		}
		
		if (shape.getRequirementSet() != null) {
			shapeManagementDTO.setRequirementSetId(shape.getRequirementSet().getId());
			shapeManagementDTO.setRequirementSetName(shape.getRequirementSet().getName());
		}

		if (shape.getKid() != null) {
			shapeManagementDTO.setKidId(shape.getKid().getId());
			shapeManagementDTO.setKidLogin(shape.getKid().getLogin());
		}

		if (shape.getCreateBy() != null) {
			shapeManagementDTO.setCreateById(shape.getCreateBy().getId());
			shapeManagementDTO.setCreateByLogin(shape.getCreateBy().getLogin());
		}
		shapeManagementDTO.setCreateAt(shape.getCreateAt());

		if (shape.getUpdateBy() != null) {
			shapeManagementDTO.setUpdateById(shape.getUpdateBy().getId());
			shapeManagementDTO.setUpdateByLogin(shape.getUpdateBy().getLogin());
		}
		shapeManagementDTO.setUpdateAt(shape.getUpdateAt());
		shapeManagementDTO.setIsDeleted(shape.isIsDeleted());

		return shapeManagementDTO;
	}

//    @Override
	public List<ShapeManagementDTO> toShapeManagementDTOs(List<Shape> shapes) {
		if (shapes == null) {
			return null;
		}

		List<ShapeManagementDTO> listShapeManagementDTOs = shapes.stream().map(this::toShapeManagementDTO)
				.collect(Collectors.toList());

		return listShapeManagementDTOs;
	}

    private String shapeKidLogin(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User kid = shape.getKid();
        if ( kid == null ) {
            return null;
        }
        String login = kid.getLogin();
        if ( login == null ) {
            return null;
        }
        return login;
    }

    private String shapeCreateByLogin(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User createBy = shape.getCreateBy();
        if ( createBy == null ) {
            return null;
        }
        String login = createBy.getLogin();
        if ( login == null ) {
            return null;
        }
        return login;
    }

    private String shapeShapeCategoryName(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        ShapeCategory shapeCategory = shape.getShapeCategory();
        if ( shapeCategory == null ) {
            return null;
        }
        String name = shapeCategory.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private Long shapeKidId(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User kid = shape.getKid();
        if ( kid == null ) {
            return null;
        }
        Long id = kid.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long shapeShapeCategoryId(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        ShapeCategory shapeCategory = shape.getShapeCategory();
        if ( shapeCategory == null ) {
            return null;
        }
        Long id = shapeCategory.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long shapeCreateById(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User createBy = shape.getCreateBy();
        if ( createBy == null ) {
            return null;
        }
        Long id = createBy.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String shapeUpdateByLogin(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User updateBy = shape.getUpdateBy();
        if ( updateBy == null ) {
            return null;
        }
        String login = updateBy.getLogin();
        if ( login == null ) {
            return null;
        }
        return login;
    }

    private Long shapeUpdateById(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        User updateBy = shape.getUpdateBy();
        if ( updateBy == null ) {
            return null;
        }
        Long id = updateBy.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
    
    private Long shapeRequirementSetId(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        RequirementSet requirementSet = shape.getRequirementSet();
        if ( requirementSet == null ) {
            return null;
        }
        Long id = requirementSet.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
    
    private String shapeRequirementSetName(Shape shape) {
        if ( shape == null ) {
            return null;
        }
        RequirementSet requirementSet = shape.getRequirementSet();
        if ( requirementSet == null ) {
            return null;
        }
        String name = requirementSet.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }
}
