package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.RequirementSetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RequirementSet} and its DTO {@link RequirementSetDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShapeCategoryMapper.class, RequirementMapper.class})
public interface RequirementSetMapper extends EntityMapper<RequirementSetDTO, RequirementSet> {

    @Mapping(source = "shapeCategory.id", target = "shapeCategoryId")
    @Mapping(source = "shapeCategory.name", target = "shapeCategoryName")
    RequirementSetDTO toDto(RequirementSet requirementSet);

    @Mapping(source = "shapeCategoryId", target = "shapeCategory")
    @Mapping(target = "removeRequirements", ignore = true)
    RequirementSet toEntity(RequirementSetDTO requirementSetDTO);

    default RequirementSet fromId(Long id) {
        if (id == null) {
            return null;
        }
        RequirementSet requirementSet = new RequirementSet();
        requirementSet.setId(id);
        return requirementSet;
    }
}
