package com.shape.service.mapper;

import com.shape.domain.*;
import com.shape.service.dto.OtherIdentifyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OtherIdentify} and its DTO {@link OtherIdentifyDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShapeCategoryMapper.class})
public interface OtherIdentifyMapper extends EntityMapper<OtherIdentifyDTO, OtherIdentify> {

    @Mapping(source = "shapeCategory.id", target = "shapeCategoryId")
    @Mapping(source = "shapeCategory.name", target = "shapeCategoryName")
    @Mapping(source = "otherIdentify.id", target = "otherIdentifyId")
    @Mapping(source = "otherIdentify.name", target = "otherIdentifyName")
    OtherIdentifyDTO toDto(OtherIdentify otherIdentify);

    @Mapping(source = "shapeCategoryId", target = "shapeCategory")
    @Mapping(source = "otherIdentifyId", target = "otherIdentify")
    OtherIdentify toEntity(OtherIdentifyDTO otherIdentifyDTO);

    default OtherIdentify fromId(Long id) {
        if (id == null) {
            return null;
        }
        OtherIdentify otherIdentify = new OtherIdentify();
        otherIdentify.setId(id);
        return otherIdentify;
    }
}
