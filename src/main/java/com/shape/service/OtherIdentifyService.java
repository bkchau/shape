package com.shape.service;

import com.shape.service.dto.OtherIdentifyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.OtherIdentify}.
 */
public interface OtherIdentifyService {

    /**
     * Save a otherIdentify.
     *
     * @param otherIdentifyDTO the entity to save.
     * @return the persisted entity.
     */
    OtherIdentifyDTO save(OtherIdentifyDTO otherIdentifyDTO);

    /**
     * Get all the otherIdentifies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OtherIdentifyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" otherIdentify.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherIdentifyDTO> findOne(Long id);

    /**
     * Delete the "id" otherIdentify.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
