package com.shape.service;

import com.shape.service.dto.RequirementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.Requirement}.
 */
public interface RequirementService {

    /**
     * Save a requirement.
     *
     * @param requirementDTO the entity to save.
     * @return the persisted entity.
     */
    RequirementDTO save(RequirementDTO requirementDTO);

    /**
     * Get all the requirements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RequirementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" requirement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequirementDTO> findOne(Long id);

    /**
     * Delete the "id" requirement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
