package com.shape.service;

import com.shape.service.dto.ShapeCategoryDTO;
import com.shape.service.dto.ShapeCategoryShapeManagementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.ShapeCategory}.
 */
public interface ShapeCategoryService {

    /**
     * Save a shapeCategory.
     *
     * @param shapeCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    ShapeCategoryDTO save(ShapeCategoryDTO shapeCategoryDTO);

    /**
     * Get all the shapeCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShapeCategoryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shapeCategory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShapeCategoryDTO> findOne(Long id);

    /**
     * Delete the "id" shapeCategory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<ShapeCategoryShapeManagementDTO> getListShapeCategoryForShapeManagement();
}
