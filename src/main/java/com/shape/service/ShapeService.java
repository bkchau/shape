package com.shape.service;

import com.shape.service.dto.ShapeDTO;
import com.shape.service.dto.ShapeManagementDTO;
import com.shape.web.rest.vm.ShapeVM;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.shape.domain.Shape}.
 */
public interface ShapeService {

    /**
     * Save a shape.
     *
     * @param shapeDTO the entity to save.
     * @return the persisted entity.
     */
    ShapeDTO save(ShapeDTO shapeDTO);

    /**
     * Get all the shapes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShapeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shape.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShapeDTO> findOne(Long id);

    /**
     * Delete the "id" shape.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<ShapeManagementDTO> getAllShapeManagement(Long kidId);
    
    ShapeManagementDTO getShapeManagement(Long id);
    
    ShapeManagementDTO submitShapeManagement(ShapeVM shapeVM);
    
    ShapeManagementDTO saveOrUpdateShapeManagement(ShapeVM shapeVM);
    
    ShapeManagementDTO deleteShapeManagement(Long id);
}
