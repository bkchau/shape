package com.shape.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.Requirement} entity.
 */
public class RequirementDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String formulaKey;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormulaKey() {
        return formulaKey;
    }

    public void setFormulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementDTO requirementDTO = (RequirementDTO) o;
        if (requirementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), requirementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RequirementDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", formulaKey='" + getFormulaKey() + "'" +
            "}";
    }
}
