package com.shape.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.OtherIdentify} entity.
 */
public class OtherIdentifyDTO implements Serializable {

    private Long id;


    private Long shapeCategoryId;

    private String shapeCategoryName;

    private Long otherIdentifyId;

    private String otherIdentifyName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShapeCategoryId() {
        return shapeCategoryId;
    }

    public void setShapeCategoryId(Long shapeCategoryId) {
        this.shapeCategoryId = shapeCategoryId;
    }

    public String getShapeCategoryName() {
        return shapeCategoryName;
    }

    public void setShapeCategoryName(String shapeCategoryName) {
        this.shapeCategoryName = shapeCategoryName;
    }

    public Long getOtherIdentifyId() {
        return otherIdentifyId;
    }

    public void setOtherIdentifyId(Long shapeCategoryId) {
        this.otherIdentifyId = shapeCategoryId;
    }

    public String getOtherIdentifyName() {
        return otherIdentifyName;
    }

    public void setOtherIdentifyName(String shapeCategoryName) {
        this.otherIdentifyName = shapeCategoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OtherIdentifyDTO otherIdentifyDTO = (OtherIdentifyDTO) o;
        if (otherIdentifyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), otherIdentifyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OtherIdentifyDTO{" +
            "id=" + getId() +
            ", shapeCategory=" + getShapeCategoryId() +
            ", shapeCategory='" + getShapeCategoryName() + "'" +
            ", otherIdentify=" + getOtherIdentifyId() +
            ", otherIdentify='" + getOtherIdentifyName() + "'" +
            "}";
    }
}
