package com.shape.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.ShapeRequirement} entity.
 */
public class ShapeRequirementDTO implements Serializable {

    private Long id;

    @NotNull
    private Float value;

    @NotNull
    private String unit;


    private Long shapeId;

    private String shapeName;

    private Long requirementId;

    private String requirementName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getShapeId() {
        return shapeId;
    }

    public void setShapeId(Long shapeId) {
        this.shapeId = shapeId;
    }

    public String getShapeName() {
        return shapeName;
    }

    public void setShapeName(String shapeName) {
        this.shapeName = shapeName;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public String getRequirementName() {
        return requirementName;
    }

    public void setRequirementName(String requirementName) {
        this.requirementName = requirementName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShapeRequirementDTO shapeRequirementDTO = (ShapeRequirementDTO) o;
        if (shapeRequirementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shapeRequirementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShapeRequirementDTO{" +
            "id=" + getId() +
            ", value=" + getValue() +
            ", unit='" + getUnit() + "'" +
            ", shape=" + getShapeId() +
            ", shape='" + getShapeName() + "'" +
            ", requirement=" + getRequirementId() +
            ", requirement='" + getRequirementName() + "'" +
            "}";
    }
}
