package com.shape.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.Shape} entity.
 */
public class ShapeDTO implements Serializable {

    private Long id;

    private String name;

    private Float area;

    private LocalDate createAt;

    private LocalDate updateAt;

    private Boolean isDeleted;


    private Long shapeCategoryId;

    private String shapeCategoryName;

    private Long kidId;

    private String kidLogin;

    private Long createById;

    private String createByLogin;

    private Long updateById;

    private String updateByLogin;

    private Long requirementSetId;

    private String requirementSetName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public LocalDate getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDate createAt) {
        this.createAt = createAt;
    }

    public LocalDate getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDate updateAt) {
        this.updateAt = updateAt;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getShapeCategoryId() {
        return shapeCategoryId;
    }

    public void setShapeCategoryId(Long shapeCategoryId) {
        this.shapeCategoryId = shapeCategoryId;
    }

    public String getShapeCategoryName() {
        return shapeCategoryName;
    }

    public void setShapeCategoryName(String shapeCategoryName) {
        this.shapeCategoryName = shapeCategoryName;
    }

    public Long getKidId() {
        return kidId;
    }

    public void setKidId(Long userId) {
        this.kidId = userId;
    }

    public String getKidLogin() {
        return kidLogin;
    }

    public void setKidLogin(String userLogin) {
        this.kidLogin = userLogin;
    }

    public Long getCreateById() {
        return createById;
    }

    public void setCreateById(Long userId) {
        this.createById = userId;
    }

    public String getCreateByLogin() {
        return createByLogin;
    }

    public void setCreateByLogin(String userLogin) {
        this.createByLogin = userLogin;
    }

    public Long getUpdateById() {
        return updateById;
    }

    public void setUpdateById(Long userId) {
        this.updateById = userId;
    }

    public String getUpdateByLogin() {
        return updateByLogin;
    }

    public void setUpdateByLogin(String userLogin) {
        this.updateByLogin = userLogin;
    }

    public Long getRequirementSetId() {
        return requirementSetId;
    }

    public void setRequirementSetId(Long requirementSetId) {
        this.requirementSetId = requirementSetId;
    }

    public String getRequirementSetName() {
        return requirementSetName;
    }

    public void setRequirementSetName(String requirementSetName) {
        this.requirementSetName = requirementSetName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShapeDTO shapeDTO = (ShapeDTO) o;
        if (shapeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shapeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShapeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", area=" + getArea() +
            ", createAt='" + getCreateAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", shapeCategory=" + getShapeCategoryId() +
            ", shapeCategory='" + getShapeCategoryName() + "'" +
            ", kid=" + getKidId() +
            ", kid='" + getKidLogin() + "'" +
            ", createBy=" + getCreateById() +
            ", createBy='" + getCreateByLogin() + "'" +
            ", updateBy=" + getUpdateById() +
            ", updateBy='" + getUpdateByLogin() + "'" +
            ", requirementSet=" + getRequirementSetId() +
            ", requirementSet='" + getRequirementSetName() + "'" +
            "}";
    }
}
