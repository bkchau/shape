package com.shape.service.dto;

import java.util.List;

public class ShapeCategoryShapeManagementDTO extends ShapeCategoryDTO {
	List<RequirementSetDTO> requirementSet;

	public List<RequirementSetDTO> getRequirementSet() {
		return requirementSet;
	}

	public void setRequirementSet(List<RequirementSetDTO> requirementSet) {
		this.requirementSet = requirementSet;
	}

}
