package com.shape.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.AreaFormula} entity.
 */
public class AreaFormulaDTO implements Serializable {

    private Long id;

    @NotNull
    private String type;

    private String operator;

    private String formulaKey;


    private Long requirementSetId;

    private String requirementSetName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFormulaKey() {
        return formulaKey;
    }

    public void setFormulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
    }

    public Long getRequirementSetId() {
        return requirementSetId;
    }

    public void setRequirementSetId(Long requirementSetId) {
        this.requirementSetId = requirementSetId;
    }

    public String getRequirementSetName() {
        return requirementSetName;
    }

    public void setRequirementSetName(String requirementSetName) {
        this.requirementSetName = requirementSetName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AreaFormulaDTO areaFormulaDTO = (AreaFormulaDTO) o;
        if (areaFormulaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), areaFormulaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AreaFormulaDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", operator='" + getOperator() + "'" +
            ", formulaKey='" + getFormulaKey() + "'" +
            ", requirementSet=" + getRequirementSetId() +
            ", requirementSet='" + getRequirementSetName() + "'" +
            "}";
    }
}
