package com.shape.service.dto;

import java.util.List;

public class ShapeManagementDTO extends ShapeDTO {
	List<OtherIdentifyDTO> otherIdenfifies;

	List<ShapeRequirementDTO> shapeRequirements;

	public List<OtherIdentifyDTO> getOtherIdenfifies() {
		return otherIdenfifies;
	}

	public void setOtherIdenfifies(List<OtherIdentifyDTO> otherIdenfifies) {
		this.otherIdenfifies = otherIdenfifies;
	}

	public List<ShapeRequirementDTO> getShapeRequirements() {
		return shapeRequirements;
	}

	public void setShapeRequirements(List<ShapeRequirementDTO> shapeRequirements) {
		this.shapeRequirements = shapeRequirements;
	}

}
