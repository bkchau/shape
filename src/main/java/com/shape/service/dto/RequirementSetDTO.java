package com.shape.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.shape.domain.RequirementSet} entity.
 */
public class RequirementSetDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;


    private Long shapeCategoryId;

    private String shapeCategoryName;

    private Set<RequirementDTO> requirements = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getShapeCategoryId() {
        return shapeCategoryId;
    }

    public void setShapeCategoryId(Long shapeCategoryId) {
        this.shapeCategoryId = shapeCategoryId;
    }

    public String getShapeCategoryName() {
        return shapeCategoryName;
    }

    public void setShapeCategoryName(String shapeCategoryName) {
        this.shapeCategoryName = shapeCategoryName;
    }

    public Set<RequirementDTO> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<RequirementDTO> requirements) {
        this.requirements = requirements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementSetDTO requirementSetDTO = (RequirementSetDTO) o;
        if (requirementSetDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), requirementSetDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RequirementSetDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", shapeCategory=" + getShapeCategoryId() +
            ", shapeCategory='" + getShapeCategoryName() + "'" +
            "}";
    }
}
