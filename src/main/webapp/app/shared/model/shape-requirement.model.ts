export interface IShapeRequirement {
  id?: number;
  value?: number;
  unit?: string;
  shapeName?: string;
  shapeId?: number;
  requirementName?: string;
  requirementId?: number;
}

export class ShapeRequirement implements IShapeRequirement {
  constructor(
    public id?: number,
    public value?: number,
    public unit?: string,
    public shapeName?: string,
    public shapeId?: number,
    public requirementName?: string,
    public requirementId?: number
  ) {}
}
