import { IRequirementSet } from 'app/shared/model/requirement-set.model';

export interface IShapeCategoryManagement {
  id?: number;
  name?: string;
  requirementSet?: IRequirementSet[];
}

export class ShapeCategoryManagement implements IShapeCategoryManagement {
  constructor(public id?: number, public name?: string, public requirementSet?: IRequirementSet[]) {}
}
