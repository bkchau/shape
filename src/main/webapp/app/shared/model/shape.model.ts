import { Moment } from 'moment';

export interface IShape {
  id?: number;
  name?: string;
  area?: number;
  createAt?: Moment;
  updateAt?: Moment;
  isDeleted?: boolean;
  shapeCategoryName?: string;
  shapeCategoryId?: number;
  kidLogin?: string;
  kidId?: number;
  createByLogin?: string;
  createById?: number;
  updateByLogin?: string;
  updateById?: number;
  requirementSetName?: string;
  requirementSetId?: number;
}

export class Shape implements IShape {
  constructor(
    public id?: number,
    public name?: string,
    public area?: number,
    public createAt?: Moment,
    public updateAt?: Moment,
    public isDeleted?: boolean,
    public shapeCategoryName?: string,
    public shapeCategoryId?: number,
    public kidLogin?: string,
    public kidId?: number,
    public createByLogin?: string,
    public createById?: number,
    public updateByLogin?: string,
    public updateById?: number,
    public requirementSetName?: string,
    public requirementSetId?: number
  ) {
    this.isDeleted = this.isDeleted || false;
  }
}
