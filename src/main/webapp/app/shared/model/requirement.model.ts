import { IRequirementSet } from 'app/shared/model/requirement-set.model';

export interface IRequirement {
  id?: number;
  name?: string;
  formulaKey?: string;
  requirementSets?: IRequirementSet[];
}

export class Requirement implements IRequirement {
  constructor(public id?: number, public name?: string, public formulaKey?: string, public requirementSets?: IRequirementSet[]) {}
}
