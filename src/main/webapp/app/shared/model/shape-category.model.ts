export interface IShapeCategory {
  id?: number;
  name?: string;
}

export class ShapeCategory implements IShapeCategory {
  constructor(public id?: number, public name?: string) {}
}
