import { IRequirement } from 'app/shared/model/requirement.model';

export interface IRequirementSet {
  id?: number;
  name?: string;
  shapeCategoryName?: string;
  shapeCategoryId?: number;
  requirements?: IRequirement[];
}

export class RequirementSet implements IRequirementSet {
  constructor(
    public id?: number,
    public name?: string,
    public shapeCategoryName?: string,
    public shapeCategoryId?: number,
    public requirements?: IRequirement[]
  ) {}
}
