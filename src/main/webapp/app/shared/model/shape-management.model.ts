import { Moment } from 'moment';

import { IOtherIdentify } from 'app/shared/model/other-identify.model';
import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';
import { IShape } from 'app/shared/model/shape.model';

export interface IShapeManagement extends IShape {
  shapeRequirements?: IShapeRequirement[];
  otherIdenfifies?: IOtherIdentify[];
}

export class ShapeManagement implements IShapeManagement {
  constructor(
    public id?: number,
    public name?: string,
    public area?: number,
    public createAt?: Moment,
    public updateAt?: Moment,
    public shapeCategoryName?: string,
    public shapeCategoryId?: number,
    public kidLogin?: string,
    public kidId?: number,
    public createByLogin?: string,
    public createById?: number,
    public updateByLogin?: string,
    public updateById?: number,
    public shapeRequirements?: IShapeRequirement[],
    public otherIdenfifies?: IOtherIdentify[]
  ) {}
}
