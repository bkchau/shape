export interface IOtherIdentify {
  id?: number;
  shapeCategoryName?: string;
  shapeCategoryId?: number;
  otherIdentifyName?: string;
  otherIdentifyId?: number;
}

export class OtherIdentify implements IOtherIdentify {
  constructor(
    public id?: number,
    public shapeCategoryName?: string,
    public shapeCategoryId?: number,
    public otherIdentifyName?: string,
    public otherIdentifyId?: number
  ) {}
}
