export interface IAreaFormula {
  id?: number;
  type?: string;
  operator?: string;
  formulaKey?: string;
  requirementSetName?: string;
  requirementSetId?: number;
}

export class AreaFormula implements IAreaFormula {
  constructor(
    public id?: number,
    public type?: string,
    public operator?: string,
    public formulaKey?: string,
    public requirementSetName?: string,
    public requirementSetId?: number
  ) {}
}
