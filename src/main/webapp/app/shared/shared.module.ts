import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ShapeSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [ShapeSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [ShapeSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeSharedModule {
  static forRoot() {
    return {
      ngModule: ShapeSharedModule
    };
  }
}
