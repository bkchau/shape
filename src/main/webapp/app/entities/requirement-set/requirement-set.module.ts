import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  RequirementSetComponent,
  RequirementSetDetailComponent,
  RequirementSetUpdateComponent,
  RequirementSetDeletePopupComponent,
  RequirementSetDeleteDialogComponent,
  requirementSetRoute,
  requirementSetPopupRoute
} from './';

const ENTITY_STATES = [...requirementSetRoute, ...requirementSetPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RequirementSetComponent,
    RequirementSetDetailComponent,
    RequirementSetUpdateComponent,
    RequirementSetDeleteDialogComponent,
    RequirementSetDeletePopupComponent
  ],
  entryComponents: [
    RequirementSetComponent,
    RequirementSetUpdateComponent,
    RequirementSetDeleteDialogComponent,
    RequirementSetDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeRequirementSetModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
