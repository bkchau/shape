import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRequirementSet } from 'app/shared/model/requirement-set.model';

@Component({
  selector: 'jhi-requirement-set-detail',
  templateUrl: './requirement-set-detail.component.html'
})
export class RequirementSetDetailComponent implements OnInit {
  requirementSet: IRequirementSet;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ requirementSet }) => {
      this.requirementSet = requirementSet;
    });
  }

  previousState() {
    window.history.back();
  }
}
