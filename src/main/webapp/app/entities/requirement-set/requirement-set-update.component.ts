import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRequirementSet, RequirementSet } from 'app/shared/model/requirement-set.model';
import { RequirementSetService } from './requirement-set.service';
import { IShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from 'app/entities/shape-category';
import { IRequirement } from 'app/shared/model/requirement.model';
import { RequirementService } from 'app/entities/requirement';

@Component({
  selector: 'jhi-requirement-set-update',
  templateUrl: './requirement-set-update.component.html'
})
export class RequirementSetUpdateComponent implements OnInit {
  isSaving: boolean;

  shapecategories: IShapeCategory[];

  requirements: IRequirement[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    shapeCategoryId: [null, Validators.required],
    requirements: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected requirementSetService: RequirementSetService,
    protected shapeCategoryService: ShapeCategoryService,
    protected requirementService: RequirementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ requirementSet }) => {
      this.updateForm(requirementSet);
    });
    this.shapeCategoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShapeCategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShapeCategory[]>) => response.body)
      )
      .subscribe((res: IShapeCategory[]) => (this.shapecategories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.requirementService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRequirement[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRequirement[]>) => response.body)
      )
      .subscribe((res: IRequirement[]) => (this.requirements = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(requirementSet: IRequirementSet) {
    this.editForm.patchValue({
      id: requirementSet.id,
      name: requirementSet.name,
      shapeCategoryId: requirementSet.shapeCategoryId,
      requirements: requirementSet.requirements
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const requirementSet = this.createFromForm();
    if (requirementSet.id !== undefined) {
      this.subscribeToSaveResponse(this.requirementSetService.update(requirementSet));
    } else {
      this.subscribeToSaveResponse(this.requirementSetService.create(requirementSet));
    }
  }

  private createFromForm(): IRequirementSet {
    return {
      ...new RequirementSet(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      shapeCategoryId: this.editForm.get(['shapeCategoryId']).value,
      requirements: this.editForm.get(['requirements']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRequirementSet>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShapeCategoryById(index: number, item: IShapeCategory) {
    return item.id;
  }

  trackRequirementById(index: number, item: IRequirement) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
