import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtherIdentify } from 'app/shared/model/other-identify.model';

@Component({
  selector: 'jhi-other-identify-detail',
  templateUrl: './other-identify-detail.component.html'
})
export class OtherIdentifyDetailComponent implements OnInit {
  otherIdentify: IOtherIdentify;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherIdentify }) => {
      this.otherIdentify = otherIdentify;
    });
  }

  previousState() {
    window.history.back();
  }
}
