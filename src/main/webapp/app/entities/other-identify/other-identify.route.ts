import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OtherIdentify } from 'app/shared/model/other-identify.model';
import { OtherIdentifyService } from './other-identify.service';
import { OtherIdentifyComponent } from './other-identify.component';
import { OtherIdentifyDetailComponent } from './other-identify-detail.component';
import { OtherIdentifyUpdateComponent } from './other-identify-update.component';
import { OtherIdentifyDeletePopupComponent } from './other-identify-delete-dialog.component';
import { IOtherIdentify } from 'app/shared/model/other-identify.model';

@Injectable({ providedIn: 'root' })
export class OtherIdentifyResolve implements Resolve<IOtherIdentify> {
  constructor(private service: OtherIdentifyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOtherIdentify> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OtherIdentify>) => response.ok),
        map((otherIdentify: HttpResponse<OtherIdentify>) => otherIdentify.body)
      );
    }
    return of(new OtherIdentify());
  }
}

export const otherIdentifyRoute: Routes = [
  {
    path: '',
    component: OtherIdentifyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.otherIdentify.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OtherIdentifyDetailComponent,
    resolve: {
      otherIdentify: OtherIdentifyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.otherIdentify.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OtherIdentifyUpdateComponent,
    resolve: {
      otherIdentify: OtherIdentifyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.otherIdentify.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OtherIdentifyUpdateComponent,
    resolve: {
      otherIdentify: OtherIdentifyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.otherIdentify.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const otherIdentifyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OtherIdentifyDeletePopupComponent,
    resolve: {
      otherIdentify: OtherIdentifyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.otherIdentify.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
