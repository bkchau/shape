import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOtherIdentify } from 'app/shared/model/other-identify.model';

type EntityResponseType = HttpResponse<IOtherIdentify>;
type EntityArrayResponseType = HttpResponse<IOtherIdentify[]>;

@Injectable({ providedIn: 'root' })
export class OtherIdentifyService {
  public resourceUrl = SERVER_API_URL + 'api/other-identifies';

  constructor(protected http: HttpClient) {}

  create(otherIdentify: IOtherIdentify): Observable<EntityResponseType> {
    return this.http.post<IOtherIdentify>(this.resourceUrl, otherIdentify, { observe: 'response' });
  }

  update(otherIdentify: IOtherIdentify): Observable<EntityResponseType> {
    return this.http.put<IOtherIdentify>(this.resourceUrl, otherIdentify, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOtherIdentify>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOtherIdentify[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
