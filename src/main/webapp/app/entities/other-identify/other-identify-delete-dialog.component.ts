import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOtherIdentify } from 'app/shared/model/other-identify.model';
import { OtherIdentifyService } from './other-identify.service';

@Component({
  selector: 'jhi-other-identify-delete-dialog',
  templateUrl: './other-identify-delete-dialog.component.html'
})
export class OtherIdentifyDeleteDialogComponent {
  otherIdentify: IOtherIdentify;

  constructor(
    protected otherIdentifyService: OtherIdentifyService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.otherIdentifyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'otherIdentifyListModification',
        content: 'Deleted an otherIdentify'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-other-identify-delete-popup',
  template: ''
})
export class OtherIdentifyDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otherIdentify }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OtherIdentifyDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.otherIdentify = otherIdentify;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/other-identify', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/other-identify', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
