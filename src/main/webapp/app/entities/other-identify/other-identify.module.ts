import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  OtherIdentifyComponent,
  OtherIdentifyDetailComponent,
  OtherIdentifyUpdateComponent,
  OtherIdentifyDeletePopupComponent,
  OtherIdentifyDeleteDialogComponent,
  otherIdentifyRoute,
  otherIdentifyPopupRoute
} from './';

const ENTITY_STATES = [...otherIdentifyRoute, ...otherIdentifyPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OtherIdentifyComponent,
    OtherIdentifyDetailComponent,
    OtherIdentifyUpdateComponent,
    OtherIdentifyDeleteDialogComponent,
    OtherIdentifyDeletePopupComponent
  ],
  entryComponents: [
    OtherIdentifyComponent,
    OtherIdentifyUpdateComponent,
    OtherIdentifyDeleteDialogComponent,
    OtherIdentifyDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeOtherIdentifyModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
