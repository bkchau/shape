export * from './other-identify.service';
export * from './other-identify-update.component';
export * from './other-identify-delete-dialog.component';
export * from './other-identify-detail.component';
export * from './other-identify.component';
export * from './other-identify.route';
