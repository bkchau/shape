import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOtherIdentify, OtherIdentify } from 'app/shared/model/other-identify.model';
import { OtherIdentifyService } from './other-identify.service';
import { IShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from 'app/entities/shape-category';

@Component({
  selector: 'jhi-other-identify-update',
  templateUrl: './other-identify-update.component.html'
})
export class OtherIdentifyUpdateComponent implements OnInit {
  isSaving: boolean;

  shapecategories: IShapeCategory[];

  editForm = this.fb.group({
    id: [],
    shapeCategoryId: [null, Validators.required],
    otherIdentifyId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected otherIdentifyService: OtherIdentifyService,
    protected shapeCategoryService: ShapeCategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ otherIdentify }) => {
      this.updateForm(otherIdentify);
    });
    this.shapeCategoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShapeCategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShapeCategory[]>) => response.body)
      )
      .subscribe((res: IShapeCategory[]) => (this.shapecategories = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(otherIdentify: IOtherIdentify) {
    this.editForm.patchValue({
      id: otherIdentify.id,
      shapeCategoryId: otherIdentify.shapeCategoryId,
      otherIdentifyId: otherIdentify.otherIdentifyId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const otherIdentify = this.createFromForm();
    if (otherIdentify.id !== undefined) {
      this.subscribeToSaveResponse(this.otherIdentifyService.update(otherIdentify));
    } else {
      this.subscribeToSaveResponse(this.otherIdentifyService.create(otherIdentify));
    }
  }

  private createFromForm(): IOtherIdentify {
    return {
      ...new OtherIdentify(),
      id: this.editForm.get(['id']).value,
      shapeCategoryId: this.editForm.get(['shapeCategoryId']).value,
      otherIdentifyId: this.editForm.get(['otherIdentifyId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtherIdentify>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShapeCategoryById(index: number, item: IShapeCategory) {
    return item.id;
  }
}
