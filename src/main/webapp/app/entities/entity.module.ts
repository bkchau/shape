import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'shape-category',
        loadChildren: './shape-category/shape-category.module#ShapeShapeCategoryModule'
      },
      {
        path: 'requirement-set',
        loadChildren: './requirement-set/requirement-set.module#ShapeRequirementSetModule'
      },
      {
        path: 'requirement',
        loadChildren: './requirement/requirement.module#ShapeRequirementModule'
      },
      {
        path: 'other-identify',
        loadChildren: './other-identify/other-identify.module#ShapeOtherIdentifyModule'
      },
      {
        path: 'area-formula',
        loadChildren: './area-formula/area-formula.module#ShapeAreaFormulaModule'
      },
      {
        path: 'shape',
        loadChildren: './shape/shape.module#ShapeShapeModule'
      },
      {
        path: 'shape-requirement',
        loadChildren: './shape-requirement/shape-requirement.module#ShapeShapeRequirementModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeEntityModule {}
