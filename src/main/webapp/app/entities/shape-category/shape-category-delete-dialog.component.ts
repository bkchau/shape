import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from './shape-category.service';

@Component({
  selector: 'jhi-shape-category-delete-dialog',
  templateUrl: './shape-category-delete-dialog.component.html'
})
export class ShapeCategoryDeleteDialogComponent {
  shapeCategory: IShapeCategory;

  constructor(
    protected shapeCategoryService: ShapeCategoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shapeCategoryService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shapeCategoryListModification',
        content: 'Deleted an shapeCategory'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shape-category-delete-popup',
  template: ''
})
export class ShapeCategoryDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shapeCategory }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShapeCategoryDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shapeCategory = shapeCategory;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shape-category', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shape-category', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
