import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShapeCategory } from 'app/shared/model/shape-category.model';

type EntityResponseType = HttpResponse<IShapeCategory>;
type EntityArrayResponseType = HttpResponse<IShapeCategory[]>;

@Injectable({ providedIn: 'root' })
export class ShapeCategoryService {
  public resourceUrl = SERVER_API_URL + 'api/shape-categories';

  constructor(protected http: HttpClient) {}

  create(shapeCategory: IShapeCategory): Observable<EntityResponseType> {
    return this.http.post<IShapeCategory>(this.resourceUrl, shapeCategory, { observe: 'response' });
  }

  update(shapeCategory: IShapeCategory): Observable<EntityResponseType> {
    return this.http.put<IShapeCategory>(this.resourceUrl, shapeCategory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShapeCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShapeCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
