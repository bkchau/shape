import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  ShapeCategoryComponent,
  ShapeCategoryDetailComponent,
  ShapeCategoryUpdateComponent,
  ShapeCategoryDeletePopupComponent,
  ShapeCategoryDeleteDialogComponent,
  shapeCategoryRoute,
  shapeCategoryPopupRoute
} from './';

const ENTITY_STATES = [...shapeCategoryRoute, ...shapeCategoryPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShapeCategoryComponent,
    ShapeCategoryDetailComponent,
    ShapeCategoryUpdateComponent,
    ShapeCategoryDeleteDialogComponent,
    ShapeCategoryDeletePopupComponent
  ],
  entryComponents: [
    ShapeCategoryComponent,
    ShapeCategoryUpdateComponent,
    ShapeCategoryDeleteDialogComponent,
    ShapeCategoryDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeShapeCategoryModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
