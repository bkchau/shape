import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IShapeCategory, ShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from './shape-category.service';

@Component({
  selector: 'jhi-shape-category-update',
  templateUrl: './shape-category-update.component.html'
})
export class ShapeCategoryUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]]
  });

  constructor(protected shapeCategoryService: ShapeCategoryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shapeCategory }) => {
      this.updateForm(shapeCategory);
    });
  }

  updateForm(shapeCategory: IShapeCategory) {
    this.editForm.patchValue({
      id: shapeCategory.id,
      name: shapeCategory.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shapeCategory = this.createFromForm();
    if (shapeCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.shapeCategoryService.update(shapeCategory));
    } else {
      this.subscribeToSaveResponse(this.shapeCategoryService.create(shapeCategory));
    }
  }

  private createFromForm(): IShapeCategory {
    return {
      ...new ShapeCategory(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShapeCategory>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
