import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from './shape-category.service';
import { ShapeCategoryComponent } from './shape-category.component';
import { ShapeCategoryDetailComponent } from './shape-category-detail.component';
import { ShapeCategoryUpdateComponent } from './shape-category-update.component';
import { ShapeCategoryDeletePopupComponent } from './shape-category-delete-dialog.component';
import { IShapeCategory } from 'app/shared/model/shape-category.model';

@Injectable({ providedIn: 'root' })
export class ShapeCategoryResolve implements Resolve<IShapeCategory> {
  constructor(private service: ShapeCategoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShapeCategory> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShapeCategory>) => response.ok),
        map((shapeCategory: HttpResponse<ShapeCategory>) => shapeCategory.body)
      );
    }
    return of(new ShapeCategory());
  }
}

export const shapeCategoryRoute: Routes = [
  {
    path: '',
    component: ShapeCategoryComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.shapeCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShapeCategoryDetailComponent,
    resolve: {
      shapeCategory: ShapeCategoryResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'shapeApp.shapeCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShapeCategoryUpdateComponent,
    resolve: {
      shapeCategory: ShapeCategoryResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'shapeApp.shapeCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShapeCategoryUpdateComponent,
    resolve: {
      shapeCategory: ShapeCategoryResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'shapeApp.shapeCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shapeCategoryPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShapeCategoryDeletePopupComponent,
    resolve: {
      shapeCategory: ShapeCategoryResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'shapeApp.shapeCategory.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
