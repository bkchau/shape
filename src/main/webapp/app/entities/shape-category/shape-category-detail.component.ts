import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShapeCategory } from 'app/shared/model/shape-category.model';

@Component({
  selector: 'jhi-shape-category-detail',
  templateUrl: './shape-category-detail.component.html'
})
export class ShapeCategoryDetailComponent implements OnInit {
  shapeCategory: IShapeCategory;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shapeCategory }) => {
      this.shapeCategory = shapeCategory;
    });
  }

  previousState() {
    window.history.back();
  }
}
