export * from './shape-category.service';
export * from './shape-category-update.component';
export * from './shape-category-delete-dialog.component';
export * from './shape-category-detail.component';
export * from './shape-category.component';
export * from './shape-category.route';
