import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAreaFormula, AreaFormula } from 'app/shared/model/area-formula.model';
import { AreaFormulaService } from './area-formula.service';
import { IRequirementSet } from 'app/shared/model/requirement-set.model';
import { RequirementSetService } from 'app/entities/requirement-set';

@Component({
  selector: 'jhi-area-formula-update',
  templateUrl: './area-formula-update.component.html'
})
export class AreaFormulaUpdateComponent implements OnInit {
  isSaving: boolean;

  requirementsets: IRequirementSet[];

  editForm = this.fb.group({
    id: [],
    type: [null, [Validators.required]],
    operator: [],
    formulaKey: [],
    requirementSetId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected areaFormulaService: AreaFormulaService,
    protected requirementSetService: RequirementSetService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ areaFormula }) => {
      this.updateForm(areaFormula);
    });
    this.requirementSetService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRequirementSet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRequirementSet[]>) => response.body)
      )
      .subscribe((res: IRequirementSet[]) => (this.requirementsets = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(areaFormula: IAreaFormula) {
    this.editForm.patchValue({
      id: areaFormula.id,
      type: areaFormula.type,
      operator: areaFormula.operator,
      formulaKey: areaFormula.formulaKey,
      requirementSetId: areaFormula.requirementSetId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const areaFormula = this.createFromForm();
    if (areaFormula.id !== undefined) {
      this.subscribeToSaveResponse(this.areaFormulaService.update(areaFormula));
    } else {
      this.subscribeToSaveResponse(this.areaFormulaService.create(areaFormula));
    }
  }

  private createFromForm(): IAreaFormula {
    return {
      ...new AreaFormula(),
      id: this.editForm.get(['id']).value,
      type: this.editForm.get(['type']).value,
      operator: this.editForm.get(['operator']).value,
      formulaKey: this.editForm.get(['formulaKey']).value,
      requirementSetId: this.editForm.get(['requirementSetId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAreaFormula>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRequirementSetById(index: number, item: IRequirementSet) {
    return item.id;
  }
}
