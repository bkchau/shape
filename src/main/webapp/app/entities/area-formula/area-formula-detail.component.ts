import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAreaFormula } from 'app/shared/model/area-formula.model';

@Component({
  selector: 'jhi-area-formula-detail',
  templateUrl: './area-formula-detail.component.html'
})
export class AreaFormulaDetailComponent implements OnInit {
  areaFormula: IAreaFormula;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ areaFormula }) => {
      this.areaFormula = areaFormula;
    });
  }

  previousState() {
    window.history.back();
  }
}
