import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AreaFormula } from 'app/shared/model/area-formula.model';
import { AreaFormulaService } from './area-formula.service';
import { AreaFormulaComponent } from './area-formula.component';
import { AreaFormulaDetailComponent } from './area-formula-detail.component';
import { AreaFormulaUpdateComponent } from './area-formula-update.component';
import { AreaFormulaDeletePopupComponent } from './area-formula-delete-dialog.component';
import { IAreaFormula } from 'app/shared/model/area-formula.model';

@Injectable({ providedIn: 'root' })
export class AreaFormulaResolve implements Resolve<IAreaFormula> {
  constructor(private service: AreaFormulaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAreaFormula> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AreaFormula>) => response.ok),
        map((areaFormula: HttpResponse<AreaFormula>) => areaFormula.body)
      );
    }
    return of(new AreaFormula());
  }
}

export const areaFormulaRoute: Routes = [
  {
    path: '',
    component: AreaFormulaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.areaFormula.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AreaFormulaDetailComponent,
    resolve: {
      areaFormula: AreaFormulaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.areaFormula.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AreaFormulaUpdateComponent,
    resolve: {
      areaFormula: AreaFormulaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.areaFormula.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AreaFormulaUpdateComponent,
    resolve: {
      areaFormula: AreaFormulaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.areaFormula.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const areaFormulaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AreaFormulaDeletePopupComponent,
    resolve: {
      areaFormula: AreaFormulaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.areaFormula.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
