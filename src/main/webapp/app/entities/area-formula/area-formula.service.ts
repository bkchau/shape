import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAreaFormula } from 'app/shared/model/area-formula.model';

type EntityResponseType = HttpResponse<IAreaFormula>;
type EntityArrayResponseType = HttpResponse<IAreaFormula[]>;

@Injectable({ providedIn: 'root' })
export class AreaFormulaService {
  public resourceUrl = SERVER_API_URL + 'api/area-formulas';

  constructor(protected http: HttpClient) {}

  create(areaFormula: IAreaFormula): Observable<EntityResponseType> {
    return this.http.post<IAreaFormula>(this.resourceUrl, areaFormula, { observe: 'response' });
  }

  update(areaFormula: IAreaFormula): Observable<EntityResponseType> {
    return this.http.put<IAreaFormula>(this.resourceUrl, areaFormula, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAreaFormula>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAreaFormula[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
