import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAreaFormula } from 'app/shared/model/area-formula.model';
import { AreaFormulaService } from './area-formula.service';

@Component({
  selector: 'jhi-area-formula-delete-dialog',
  templateUrl: './area-formula-delete-dialog.component.html'
})
export class AreaFormulaDeleteDialogComponent {
  areaFormula: IAreaFormula;

  constructor(
    protected areaFormulaService: AreaFormulaService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.areaFormulaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'areaFormulaListModification',
        content: 'Deleted an areaFormula'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-area-formula-delete-popup',
  template: ''
})
export class AreaFormulaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ areaFormula }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AreaFormulaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.areaFormula = areaFormula;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/area-formula', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/area-formula', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
