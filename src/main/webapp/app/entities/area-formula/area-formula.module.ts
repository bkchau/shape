import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  AreaFormulaComponent,
  AreaFormulaDetailComponent,
  AreaFormulaUpdateComponent,
  AreaFormulaDeletePopupComponent,
  AreaFormulaDeleteDialogComponent,
  areaFormulaRoute,
  areaFormulaPopupRoute
} from './';

const ENTITY_STATES = [...areaFormulaRoute, ...areaFormulaPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AreaFormulaComponent,
    AreaFormulaDetailComponent,
    AreaFormulaUpdateComponent,
    AreaFormulaDeleteDialogComponent,
    AreaFormulaDeletePopupComponent
  ],
  entryComponents: [AreaFormulaComponent, AreaFormulaUpdateComponent, AreaFormulaDeleteDialogComponent, AreaFormulaDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeAreaFormulaModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
