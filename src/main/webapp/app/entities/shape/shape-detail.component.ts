import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShape } from 'app/shared/model/shape.model';

@Component({
  selector: 'jhi-shape-detail',
  templateUrl: './shape-detail.component.html'
})
export class ShapeDetailComponent implements OnInit {
  shape: IShape;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shape }) => {
      this.shape = shape;
    });
  }

  previousState() {
    window.history.back();
  }
}
