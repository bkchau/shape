import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Shape } from 'app/shared/model/shape.model';
import { ShapeService } from './shape.service';
import { ShapeComponent } from './shape.component';
import { ShapeDetailComponent } from './shape-detail.component';
import { ShapeUpdateComponent } from './shape-update.component';
import { ShapeDeletePopupComponent } from './shape-delete-dialog.component';
import { IShape } from 'app/shared/model/shape.model';

@Injectable({ providedIn: 'root' })
export class ShapeResolve implements Resolve<IShape> {
  constructor(private service: ShapeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShape> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Shape>) => response.ok),
        map((shape: HttpResponse<Shape>) => shape.body)
      );
    }
    return of(new Shape());
  }
}

export const shapeRoute: Routes = [
  {
    path: '',
    component: ShapeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.shape.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShapeDetailComponent,
    resolve: {
      shape: ShapeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shape.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShapeUpdateComponent,
    resolve: {
      shape: ShapeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shape.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShapeUpdateComponent,
    resolve: {
      shape: ShapeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shape.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shapePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShapeDeletePopupComponent,
    resolve: {
      shape: ShapeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shape.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
