import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IShape, Shape } from 'app/shared/model/shape.model';
import { ShapeService } from './shape.service';
import { IShapeCategory } from 'app/shared/model/shape-category.model';
import { ShapeCategoryService } from 'app/entities/shape-category';
import { IUser, UserService } from 'app/core';
import { IRequirementSet } from 'app/shared/model/requirement-set.model';
import { RequirementSetService } from 'app/entities/requirement-set';

@Component({
  selector: 'jhi-shape-update',
  templateUrl: './shape-update.component.html'
})
export class ShapeUpdateComponent implements OnInit {
  isSaving: boolean;

  shapecategories: IShapeCategory[];

  users: IUser[];

  requirementsets: IRequirementSet[];
  createAtDp: any;
  updateAtDp: any;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    area: [],
    createAt: [null, [Validators.required]],
    updateAt: [],
    isDeleted: [],
    shapeCategoryId: [null, Validators.required],
    kidId: [],
    createById: [null, Validators.required],
    updateById: [],
    requirementSetId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shapeService: ShapeService,
    protected shapeCategoryService: ShapeCategoryService,
    protected userService: UserService,
    protected requirementSetService: RequirementSetService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shape }) => {
      this.updateForm(shape);
    });
    this.shapeCategoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShapeCategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShapeCategory[]>) => response.body)
      )
      .subscribe((res: IShapeCategory[]) => (this.shapecategories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.requirementSetService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRequirementSet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRequirementSet[]>) => response.body)
      )
      .subscribe((res: IRequirementSet[]) => (this.requirementsets = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shape: IShape) {
    this.editForm.patchValue({
      id: shape.id,
      name: shape.name,
      area: shape.area,
      createAt: shape.createAt,
      updateAt: shape.updateAt,
      isDeleted: shape.isDeleted,
      shapeCategoryId: shape.shapeCategoryId,
      kidId: shape.kidId,
      createById: shape.createById,
      updateById: shape.updateById,
      requirementSetId: shape.requirementSetId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shape = this.createFromForm();
    if (shape.id !== undefined) {
      this.subscribeToSaveResponse(this.shapeService.update(shape));
    } else {
      this.subscribeToSaveResponse(this.shapeService.create(shape));
    }
  }

  private createFromForm(): IShape {
    return {
      ...new Shape(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      area: this.editForm.get(['area']).value,
      createAt: this.editForm.get(['createAt']).value,
      updateAt: this.editForm.get(['updateAt']).value,
      isDeleted: this.editForm.get(['isDeleted']).value,
      shapeCategoryId: this.editForm.get(['shapeCategoryId']).value,
      kidId: this.editForm.get(['kidId']).value,
      createById: this.editForm.get(['createById']).value,
      updateById: this.editForm.get(['updateById']).value,
      requirementSetId: this.editForm.get(['requirementSetId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShape>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShapeCategoryById(index: number, item: IShapeCategory) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackRequirementSetById(index: number, item: IRequirementSet) {
    return item.id;
  }
}
