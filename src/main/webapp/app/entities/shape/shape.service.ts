import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShape } from 'app/shared/model/shape.model';

type EntityResponseType = HttpResponse<IShape>;
type EntityArrayResponseType = HttpResponse<IShape[]>;

@Injectable({ providedIn: 'root' })
export class ShapeService {
  public resourceUrl = SERVER_API_URL + 'api/shapes';

  constructor(protected http: HttpClient) {}

  create(shape: IShape): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shape);
    return this.http
      .post<IShape>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shape: IShape): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shape);
    return this.http
      .put<IShape>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShape>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShape[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shape: IShape): IShape {
    const copy: IShape = Object.assign({}, shape, {
      createAt: shape.createAt != null && shape.createAt.isValid() ? shape.createAt.format(DATE_FORMAT) : null,
      updateAt: shape.updateAt != null && shape.updateAt.isValid() ? shape.updateAt.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createAt = res.body.createAt != null ? moment(res.body.createAt) : null;
      res.body.updateAt = res.body.updateAt != null ? moment(res.body.updateAt) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shape: IShape) => {
        shape.createAt = shape.createAt != null ? moment(shape.createAt) : null;
        shape.updateAt = shape.updateAt != null ? moment(shape.updateAt) : null;
      });
    }
    return res;
  }
}
