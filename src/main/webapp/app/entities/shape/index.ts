export * from './shape.service';
export * from './shape-update.component';
export * from './shape-delete-dialog.component';
export * from './shape-detail.component';
export * from './shape.component';
export * from './shape.route';
