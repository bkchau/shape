import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  ShapeComponent,
  ShapeDetailComponent,
  ShapeUpdateComponent,
  ShapeDeletePopupComponent,
  ShapeDeleteDialogComponent,
  shapeRoute,
  shapePopupRoute
} from './';

const ENTITY_STATES = [...shapeRoute, ...shapePopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ShapeComponent, ShapeDetailComponent, ShapeUpdateComponent, ShapeDeleteDialogComponent, ShapeDeletePopupComponent],
  entryComponents: [ShapeComponent, ShapeUpdateComponent, ShapeDeleteDialogComponent, ShapeDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeShapeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
