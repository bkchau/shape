import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShape } from 'app/shared/model/shape.model';
import { ShapeService } from './shape.service';

@Component({
  selector: 'jhi-shape-delete-dialog',
  templateUrl: './shape-delete-dialog.component.html'
})
export class ShapeDeleteDialogComponent {
  shape: IShape;

  constructor(protected shapeService: ShapeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shapeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shapeListModification',
        content: 'Deleted an shape'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shape-delete-popup',
  template: ''
})
export class ShapeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shape }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShapeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shape = shape;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shape', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shape', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
