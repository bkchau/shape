import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  RequirementComponent,
  RequirementDetailComponent,
  RequirementUpdateComponent,
  RequirementDeletePopupComponent,
  RequirementDeleteDialogComponent,
  requirementRoute,
  requirementPopupRoute
} from './';

const ENTITY_STATES = [...requirementRoute, ...requirementPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RequirementComponent,
    RequirementDetailComponent,
    RequirementUpdateComponent,
    RequirementDeleteDialogComponent,
    RequirementDeletePopupComponent
  ],
  entryComponents: [RequirementComponent, RequirementUpdateComponent, RequirementDeleteDialogComponent, RequirementDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeRequirementModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
