import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Requirement } from 'app/shared/model/requirement.model';
import { RequirementService } from './requirement.service';
import { RequirementComponent } from './requirement.component';
import { RequirementDetailComponent } from './requirement-detail.component';
import { RequirementUpdateComponent } from './requirement-update.component';
import { RequirementDeletePopupComponent } from './requirement-delete-dialog.component';
import { IRequirement } from 'app/shared/model/requirement.model';

@Injectable({ providedIn: 'root' })
export class RequirementResolve implements Resolve<IRequirement> {
  constructor(private service: RequirementService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRequirement> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Requirement>) => response.ok),
        map((requirement: HttpResponse<Requirement>) => requirement.body)
      );
    }
    return of(new Requirement());
  }
}

export const requirementRoute: Routes = [
  {
    path: '',
    component: RequirementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.requirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RequirementDetailComponent,
    resolve: {
      requirement: RequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.requirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RequirementUpdateComponent,
    resolve: {
      requirement: RequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.requirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RequirementUpdateComponent,
    resolve: {
      requirement: RequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.requirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const requirementPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RequirementDeletePopupComponent,
    resolve: {
      requirement: RequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.requirement.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
