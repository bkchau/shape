import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRequirement, Requirement } from 'app/shared/model/requirement.model';
import { RequirementService } from './requirement.service';
import { IRequirementSet } from 'app/shared/model/requirement-set.model';
import { RequirementSetService } from 'app/entities/requirement-set';

@Component({
  selector: 'jhi-requirement-update',
  templateUrl: './requirement-update.component.html'
})
export class RequirementUpdateComponent implements OnInit {
  isSaving: boolean;

  requirementsets: IRequirementSet[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    formulaKey: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected requirementService: RequirementService,
    protected requirementSetService: RequirementSetService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ requirement }) => {
      this.updateForm(requirement);
    });
    this.requirementSetService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRequirementSet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRequirementSet[]>) => response.body)
      )
      .subscribe((res: IRequirementSet[]) => (this.requirementsets = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(requirement: IRequirement) {
    this.editForm.patchValue({
      id: requirement.id,
      name: requirement.name,
      formulaKey: requirement.formulaKey
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const requirement = this.createFromForm();
    if (requirement.id !== undefined) {
      this.subscribeToSaveResponse(this.requirementService.update(requirement));
    } else {
      this.subscribeToSaveResponse(this.requirementService.create(requirement));
    }
  }

  private createFromForm(): IRequirement {
    return {
      ...new Requirement(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      formulaKey: this.editForm.get(['formulaKey']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRequirement>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRequirementSetById(index: number, item: IRequirementSet) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
