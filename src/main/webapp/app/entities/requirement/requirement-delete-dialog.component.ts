import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRequirement } from 'app/shared/model/requirement.model';
import { RequirementService } from './requirement.service';

@Component({
  selector: 'jhi-requirement-delete-dialog',
  templateUrl: './requirement-delete-dialog.component.html'
})
export class RequirementDeleteDialogComponent {
  requirement: IRequirement;

  constructor(
    protected requirementService: RequirementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.requirementService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'requirementListModification',
        content: 'Deleted an requirement'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-requirement-delete-popup',
  template: ''
})
export class RequirementDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ requirement }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RequirementDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.requirement = requirement;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/requirement', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/requirement', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
