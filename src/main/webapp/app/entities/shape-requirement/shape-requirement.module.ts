import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ShapeSharedModule } from 'app/shared';
import {
  ShapeRequirementComponent,
  ShapeRequirementDetailComponent,
  ShapeRequirementUpdateComponent,
  ShapeRequirementDeletePopupComponent,
  ShapeRequirementDeleteDialogComponent,
  shapeRequirementRoute,
  shapeRequirementPopupRoute
} from './';

const ENTITY_STATES = [...shapeRequirementRoute, ...shapeRequirementPopupRoute];

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShapeRequirementComponent,
    ShapeRequirementDetailComponent,
    ShapeRequirementUpdateComponent,
    ShapeRequirementDeleteDialogComponent,
    ShapeRequirementDeletePopupComponent
  ],
  entryComponents: [
    ShapeRequirementComponent,
    ShapeRequirementUpdateComponent,
    ShapeRequirementDeleteDialogComponent,
    ShapeRequirementDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeShapeRequirementModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
