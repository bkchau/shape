import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';
import { ShapeRequirementService } from './shape-requirement.service';

@Component({
  selector: 'jhi-shape-requirement-delete-dialog',
  templateUrl: './shape-requirement-delete-dialog.component.html'
})
export class ShapeRequirementDeleteDialogComponent {
  shapeRequirement: IShapeRequirement;

  constructor(
    protected shapeRequirementService: ShapeRequirementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shapeRequirementService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shapeRequirementListModification',
        content: 'Deleted an shapeRequirement'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shape-requirement-delete-popup',
  template: ''
})
export class ShapeRequirementDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shapeRequirement }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShapeRequirementDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shapeRequirement = shapeRequirement;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shape-requirement', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shape-requirement', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
