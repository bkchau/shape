import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';

type EntityResponseType = HttpResponse<IShapeRequirement>;
type EntityArrayResponseType = HttpResponse<IShapeRequirement[]>;

@Injectable({ providedIn: 'root' })
export class ShapeRequirementService {
  public resourceUrl = SERVER_API_URL + 'api/shape-requirements';

  constructor(protected http: HttpClient) {}

  create(shapeRequirement: IShapeRequirement): Observable<EntityResponseType> {
    return this.http.post<IShapeRequirement>(this.resourceUrl, shapeRequirement, { observe: 'response' });
  }

  update(shapeRequirement: IShapeRequirement): Observable<EntityResponseType> {
    return this.http.put<IShapeRequirement>(this.resourceUrl, shapeRequirement, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShapeRequirement>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShapeRequirement[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
