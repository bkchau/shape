import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShapeRequirement } from 'app/shared/model/shape-requirement.model';
import { ShapeRequirementService } from './shape-requirement.service';
import { ShapeRequirementComponent } from './shape-requirement.component';
import { ShapeRequirementDetailComponent } from './shape-requirement-detail.component';
import { ShapeRequirementUpdateComponent } from './shape-requirement-update.component';
import { ShapeRequirementDeletePopupComponent } from './shape-requirement-delete-dialog.component';
import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';

@Injectable({ providedIn: 'root' })
export class ShapeRequirementResolve implements Resolve<IShapeRequirement> {
  constructor(private service: ShapeRequirementService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShapeRequirement> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShapeRequirement>) => response.ok),
        map((shapeRequirement: HttpResponse<ShapeRequirement>) => shapeRequirement.body)
      );
    }
    return of(new ShapeRequirement());
  }
}

export const shapeRequirementRoute: Routes = [
  {
    path: '',
    component: ShapeRequirementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'shapeApp.shapeRequirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShapeRequirementDetailComponent,
    resolve: {
      shapeRequirement: ShapeRequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shapeRequirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShapeRequirementUpdateComponent,
    resolve: {
      shapeRequirement: ShapeRequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shapeRequirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShapeRequirementUpdateComponent,
    resolve: {
      shapeRequirement: ShapeRequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shapeRequirement.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shapeRequirementPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShapeRequirementDeletePopupComponent,
    resolve: {
      shapeRequirement: ShapeRequirementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'shapeApp.shapeRequirement.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
