import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShapeRequirement, ShapeRequirement } from 'app/shared/model/shape-requirement.model';
import { ShapeRequirementService } from './shape-requirement.service';
import { IShape } from 'app/shared/model/shape.model';
import { ShapeService } from 'app/entities/shape';
import { IRequirement } from 'app/shared/model/requirement.model';
import { RequirementService } from 'app/entities/requirement';

@Component({
  selector: 'jhi-shape-requirement-update',
  templateUrl: './shape-requirement-update.component.html'
})
export class ShapeRequirementUpdateComponent implements OnInit {
  isSaving: boolean;

  shapes: IShape[];

  requirements: IRequirement[];

  editForm = this.fb.group({
    id: [],
    value: [null, [Validators.required]],
    unit: [null, [Validators.required]],
    shapeId: [null, Validators.required],
    requirementId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shapeRequirementService: ShapeRequirementService,
    protected shapeService: ShapeService,
    protected requirementService: RequirementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shapeRequirement }) => {
      this.updateForm(shapeRequirement);
    });
    this.shapeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShape[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShape[]>) => response.body)
      )
      .subscribe((res: IShape[]) => (this.shapes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.requirementService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRequirement[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRequirement[]>) => response.body)
      )
      .subscribe((res: IRequirement[]) => (this.requirements = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shapeRequirement: IShapeRequirement) {
    this.editForm.patchValue({
      id: shapeRequirement.id,
      value: shapeRequirement.value,
      unit: shapeRequirement.unit,
      shapeId: shapeRequirement.shapeId,
      requirementId: shapeRequirement.requirementId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shapeRequirement = this.createFromForm();
    if (shapeRequirement.id !== undefined) {
      this.subscribeToSaveResponse(this.shapeRequirementService.update(shapeRequirement));
    } else {
      this.subscribeToSaveResponse(this.shapeRequirementService.create(shapeRequirement));
    }
  }

  private createFromForm(): IShapeRequirement {
    return {
      ...new ShapeRequirement(),
      id: this.editForm.get(['id']).value,
      value: this.editForm.get(['value']).value,
      unit: this.editForm.get(['unit']).value,
      shapeId: this.editForm.get(['shapeId']).value,
      requirementId: this.editForm.get(['requirementId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShapeRequirement>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShapeById(index: number, item: IShape) {
    return item.id;
  }

  trackRequirementById(index: number, item: IRequirement) {
    return item.id;
  }
}
