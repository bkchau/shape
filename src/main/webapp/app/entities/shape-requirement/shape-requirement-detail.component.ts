import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';

@Component({
  selector: 'jhi-shape-requirement-detail',
  templateUrl: './shape-requirement-detail.component.html'
})
export class ShapeRequirementDetailComponent implements OnInit {
  shapeRequirement: IShapeRequirement;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shapeRequirement }) => {
      this.shapeRequirement = shapeRequirement;
    });
  }

  previousState() {
    window.history.back();
  }
}
