export * from './shape-requirement.service';
export * from './shape-requirement-update.component';
export * from './shape-requirement-delete-dialog.component';
export * from './shape-requirement-detail.component';
export * from './shape-requirement.component';
export * from './shape-requirement.route';
