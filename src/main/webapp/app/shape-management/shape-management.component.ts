import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Account, AccountService, UserService, IUser } from 'app/core';

import { ShapeManagementService } from './shape-management.service';
import { IOtherIdentify } from 'app/shared/model/other-identify.model';
import { IShapeCategoryManagement } from 'app/shared/model/shape-category-management.model';
import { IRequirementSet } from 'app/shared/model/requirement-set.model';
import { IRequirement } from 'app/shared/model/requirement.model';
import { IShapeManagement } from 'app/shared/model/shape-management.model';

@Component({
  selector: 'shape-management',
  templateUrl: './shape-management.component.html',
  styleUrls: ['shape-management.scss']
})
export class ShapeManagementComponent implements OnInit, OnDestroy {
  account: Account;

  listKids: IUser[];
  listShapeManagement: IShapeManagement[];

  form = this.fb.group({
    kid: []
  });

  constructor(
    protected shapeManagementService: ShapeManagementService,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    // protected activatedRoute: ActivatedRoute,
    protected userService: UserService,
    protected router: Router,
    // protected eventManager: JhiEventManager
    private fb: FormBuilder
  ) {}

  loadAllKids() {
    this.userService.getAllKids().subscribe(
      (res: HttpResponse<IUser[]>) => {
        this.listKids = res.body;
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  loadAllShapeManagement() {
    let kidId = null;
    const kid = this.form.get('kid').value;
    if (kid) {
      kidId = kid.id;
    }

    this.shapeManagementService.getListShapeManagement(kidId).subscribe(
      (res: HttpResponse<IShapeManagement[]>) => {
        this.listShapeManagement = res.body;
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  search() {
    this.loadAllShapeManagement();
  }

  addNew() {
    this.router.navigateByUrl('shape-management-new');
  }

  edit(shapeManagement: IShapeManagement) {
    this.router.navigateByUrl('shape-management-edit/' + shapeManagement.id);
  }

  delete(shapeManagement: IShapeManagement) {
    this.shapeManagementService.delete(shapeManagement.id).subscribe(
      (res: HttpResponse<IShapeManagement>) => {
        // reload list shape
        this.loadAllShapeManagement();
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.account = account;

      if (this.account.authorities.indexOf('ROLE_ADMIN') == -1) {
        this.form.get('kid').disable();
      }
    });

    this.loadAllKids();
    this.loadAllShapeManagement();
  }

  ngOnDestroy() {
    // this.eventManager.destroy(this.eventSubscriber);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
