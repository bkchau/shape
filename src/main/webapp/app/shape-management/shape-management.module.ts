import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShapeSharedModule } from 'app/shared';
import { shapeManagementRoute } from './shape-management.route';
import { ShapeManagementComponent } from './shape-management.component';
import { ShapeManagementDetailComponent } from './shape-management-detail.component';

@NgModule({
  imports: [ShapeSharedModule, RouterModule.forChild(shapeManagementRoute)],
  declarations: [ShapeManagementComponent, ShapeManagementDetailComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShapeManagementModule {}
