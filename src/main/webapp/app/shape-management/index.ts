export * from './shape-management.service';
export * from './shape-management.component';
export * from './shape-management-detail.component';
export * from './shape-management.route';
export * from './shape-management.module';
