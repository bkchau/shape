import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShapeCategoryManagement } from 'app/shared/model/shape-category-management.model';
import { IShapeManagement } from 'app/shared/model/shape-management.model';

type EntityResponseType = HttpResponse<IShapeManagement>;
type EntityArrayResponseType = HttpResponse<IShapeManagement[]>;

@Injectable({ providedIn: 'root' })
export class ShapeManagementService {
  public resourceUrl = SERVER_API_URL + 'api/shape-management';

  constructor(protected http: HttpClient) {}

  getListShapeCategory(): Observable<HttpResponse<IShapeCategoryManagement[]>> {
    return this.http.get<IShapeCategoryManagement[]>(`${this.resourceUrl}/get-all-shape-category`, { observe: 'response' });
  }

  getListShapeManagement(kidId: number): Observable<EntityArrayResponseType> {
    let url = 'get-all-shape-management/';
    if (kidId != null) {
      url += kidId.toString();
    }

    return this.http.get<IShapeManagement[]>(`${this.resourceUrl}/${url}`, { observe: 'response' });
  }

  getShapeManagement(id: any): Observable<EntityResponseType> {
    return this.http.get<IShapeManagement>(`${this.resourceUrl}/get-shape-management/${id}`, { observe: 'response' });
  }

  submit(shape: any): Observable<EntityResponseType> {
    return this.http.post<IShapeManagement>(`${this.resourceUrl}/submit-shape-management`, shape, { observe: 'response' });
  }

  save(shape: any): Observable<EntityResponseType> {
    return this.http.post<IShapeManagement>(`${this.resourceUrl}/save-shape-management`, shape, { observe: 'response' });
  }

  delete(id: any): Observable<EntityResponseType> {
    return this.http.delete<IShapeManagement>(`${this.resourceUrl}/delete-shape-management/${id}`, { observe: 'response' });
  }
}
