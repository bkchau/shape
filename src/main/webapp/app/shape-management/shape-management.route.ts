import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShapeManagementComponent } from './shape-management.component';
import { ShapeManagementDetailComponent } from './shape-management-detail.component';

export const shapeManagementRoute: Routes = [
  {
    path: 'shape-management',
    component: ShapeManagementComponent,
    resolve: {},
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'shape-management-new',
    component: ShapeManagementDetailComponent,
    resolve: {},
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'shape-management-edit/:id',
    component: ShapeManagementDetailComponent,
    resolve: {},
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
];
