import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { AccountService, UserService, IUser } from 'app/core';

import { ShapeManagementService } from './shape-management.service';
import { IOtherIdentify } from 'app/shared/model/other-identify.model';
import { IShapeCategoryManagement } from 'app/shared/model/shape-category-management.model';
import { IShapeManagement } from 'app/shared/model/shape-management.model';
import { IShapeRequirement } from 'app/shared/model/shape-requirement.model';
import { IRequirementSet } from 'app/shared/model/requirement-set.model';
import { IRequirement } from 'app/shared/model/requirement.model';

@Component({
  selector: 'shape-management-detail',
  templateUrl: './shape-management-detail.component.html'
})
export class ShapeManagementDetailComponent implements OnInit, OnDestroy {
  account: any;
  isAdmin = false;
  isEditMode = false;

  isSubmitted = false;
  isSubmitting = false;

  isSaving = false;

  listKids: IUser[];

  listShapeCategory: IShapeCategoryManagement[];
  listRequirementSet: IRequirementSet[];

  listOtherIdentify: IOtherIdentify[];
  area: number;
  id: any;

  form = this.fb.group({
    kidId: [],
    name: [null, Validators.required],
    shapeCategoryId: [null, Validators.required],
    requirementSetId: [null, Validators.required],
    shapeRequirement: this.fb.array([])
  });

  constructor(
    protected shapeManagementService: ShapeManagementService,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    // protected eventManager: JhiEventManager
    private fb: FormBuilder
  ) {}

  loadAllKids() {
    this.userService.getAllKids().subscribe(
      (res: HttpResponse<IUser[]>) => {
        this.listKids = res.body;
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  loadAllShapeCategory() {
    this.shapeManagementService.getListShapeCategory().subscribe(
      (res: HttpResponse<IShapeCategoryManagement[]>) => {
        this.listShapeCategory = res.body;

        if (this.isEditMode) {
          // update list requirementSet
          this.refreshListShapeCategory();
        } else if (this.listShapeCategory && this.listShapeCategory.length > 0) {
          // set default value on 'add new' mode
          this.form.controls['shapeCategoryId'].setValue(this.listShapeCategory[0].id);
          this.onShapeCategoryChange();
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  loadShapeManagement(id: any) {
    this.shapeManagementService.getShapeManagement(id).subscribe(
      (res: HttpResponse<IShapeManagement>) => {
        const shapeManagement = res.body;
        this.form.patchValue({
          name: shapeManagement.name,
          kidId: shapeManagement.kidId,
          shapeCategoryId: shapeManagement.shapeCategoryId,
          requirementSetId: shapeManagement.requirementSetId
        });

        this.refreshListShapeCategory();
        this.patchListRequirement(shapeManagement.shapeRequirements);

        // admin can not change ShapeCategory and RequirementSet
        this.form.get('shapeCategoryId').disable();
        this.form.get('requirementSetId').disable();
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  onShapeCategoryChange() {
    this.refreshListShapeCategory();

    if (this.listRequirementSet && this.listRequirementSet.length > 0) {
      this.form.controls['requirementSetId'].setValue(this.listRequirementSet[0].id);
      this.onRequirementSetChange();
    } else {
      // remove old list
      const listRequirement = this.form.get('shapeRequirement') as FormArray;
      listRequirement.controls = [];
    }
  }

  onRequirementSetChange() {
    let listRequirement = null;
    const requirementId = this.form.get('requirementSetId').value;
    if (this.listRequirementSet && requirementId) {
      this.listRequirementSet.forEach(rs => {
        if (rs.id === requirementId) {
          listRequirement = rs.requirements;
        }
      });
    }

    const listRequirementFA = this.form.get('shapeRequirement') as FormArray;

    // remove old list
    listRequirementFA.controls = [];

    if (listRequirement && listRequirement.length > 0) {
      listRequirement.forEach(requirement => {
        listRequirementFA.push(
          this.fb.group({
            id: [],
            requirementId: requirement.id,
            name: requirement.name,
            value: [null, [Validators.required, Validators.pattern('^[0-9]+(.[0-9]+)?$')]],
            unit: [null, Validators.required]
          })
        );
      });
    }
  }

  backToListPage() {
    this.router.navigateByUrl('shape-management');
  }

  submit() {
    this.isSubmitting = true;

    const shapeVM = this.buildParameter();

    this.shapeManagementService.submit(shapeVM).subscribe(
      (res: HttpResponse<IShapeManagement>) => {
        this.isSubmitted = true;
        this.isSubmitting = false;

        if (res.body) {
          this.area = res.body.area;
          this.listOtherIdentify = res.body.otherIdenfifies;
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  save() {
    this.isSaving = true;

    const shapeVM = this.buildParameter();

    this.shapeManagementService.save(shapeVM).subscribe(
      (res: HttpResponse<IShapeManagement>) => {
        this.isSaving = false;

        // move to list page
        this.router.navigateByUrl('shape-management');
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.account = account;

      if (this.account.authorities.indexOf('ROLE_ADMIN') == -1) {
        this.form.get('kidId').disable();
      } else {
        this.isAdmin = true;
      }
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
      if (this.id) {
        this.isEditMode = true;
        this.loadShapeManagement(this.id);
      }

      this.loadAllKids();
      this.loadAllShapeCategory();
    });
  }

  ngOnDestroy() {
    // this.eventManager.destroy(this.eventSubscriber);
  }

  protected buildParameter() {
    // shapeRequirement
    let listShapeRequirement = [];
    let listShapeRequirementControls = this.form.get('shapeRequirement') as FormArray;
    for (let i = 0; i < listShapeRequirementControls.length; i++) {
      let shapeRequirementControls = listShapeRequirementControls.at(i);
      let shapeRequirement = {
        id: shapeRequirementControls.get('id').value,
        requirementId: shapeRequirementControls.get('requirementId').value,
        value: shapeRequirementControls.get('value').value,
        unit: shapeRequirementControls.get('unit').value
      };
      listShapeRequirement.push(shapeRequirement);
    }

    let shapeVM = {
      id: this.id,
      name: this.form.get('name').value,
      shapeCategoryId: this.form.get('shapeCategoryId').value,
      requirementSetId: this.form.get('requirementSetId').value,
      kidId: this.form.get('kidId').value,
      listShapeRequirement: listShapeRequirement,
      createAt: new Date()
    };

    return shapeVM;
  }

  protected refreshListShapeCategory() {
    const shapeCategoryId = this.form.get('shapeCategoryId').value;

    if (this.listShapeCategory && shapeCategoryId) {
      this.listShapeCategory.forEach(sc => {
        if (sc.id === shapeCategoryId) {
          this.listRequirementSet = sc.requirementSet;
        }
      });
    }
  }

  protected patchListRequirement(shapeRequirements: IShapeRequirement[]) {
    const listRequirementFA = this.form.get('shapeRequirement') as FormArray;

    // remove old list
    listRequirementFA.controls = [];

    if (shapeRequirements && shapeRequirements.length > 0) {
      shapeRequirements.forEach(shapeRequirement => {
        listRequirementFA.push(
          this.fb.group({
            id: shapeRequirement.id,
            requirementId: shapeRequirement.requirementId,
            name: shapeRequirement.requirementName,
            value: [shapeRequirement.value, [Validators.required, Validators.pattern('^[0-9]+(.[0-9]+)?$')]],
            unit: [shapeRequirement.unit, Validators.required]
          })
        );
      });
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
