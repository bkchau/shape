Hi,

I used JHipster to build this project. You can find more information at [https://www.jhipster.tech/](https://www.jhipster.tech/).

## Install Enviroment

You need to install enviroment before you run this project. You need to install:

1. Git (Optional)
2. Gradle (If any, please use version 5.5)
3. MySQL
4. Java 11 (If any, please use version 11.0.3 or higher)
5. Nodejs (please use an LTS 64-bit version) (If any, please use version v10.16.0)
6. npm (If any, please use version 6.9.0)

## Download source code

- You can download source code at [https://bitbucket.org/bkchau/shape/src/master/](https://bitbucket.org/bkchau/shape/src/master/).
- Or you can download by git command line:

```
git clone https://bkchau@bitbucket.org/bkchau/shape.git
```

I already publiced this project.

## Set up project

Please change database connection config at lines 39, 40 and 41 of [src/main/resources/config/application-dev.yml](src/main/resources/config/application-dev.yml).

Example:

```
url: jdbc:mysql://localhost:3306/Shape?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC
username: root
password: 123456
```

Please manualy create a schema in MySQL by name `Shape`.

## Run project

You can run back-end site by command line:

```
cd shape
./gradlew
```

You can run front-end site by command line:

```
cd shape
npm install
npm start
```

Please access web site at link [http://localhost:9000](http://localhost:9000).
I already set up:

- Account for administrator: username `admin` and password `admin`
- Account for kid: username `kid` and password `shapekid`

Please try to manage Shape at link [http://localhost:9000/shape-management](http://localhost:9000/shape-management). You can click on navigation bar as well.

## Task's status

### Basic requirement

I finished all basic requirements

- API to list all the ​shape​ ​categorie and its corresponding requirement sets: [http://localhost:8080/api/shape-management/get-all-shape-category](http://localhost:8080/api/shape-management/get-all-shape-category).
- API to submit the ​shape​, returning with ​area​ and possible ​categories​: [http://localhost:8080/api/shape-management/submit-shape-management](http://localhost:8080/api/shape-management/submit-shape-management).
- API to save the ​shape: [http://localhost:8080/api/shape-management/save-shape-management](http://localhost:8080/api/shape-management/save-shape-management).
- API to list all the saved ​shapes: [http://localhost:8080/api/shape-management/get-all-shape-management/{kidId}](http://localhost:8080/api/shape-management/get-all-shape-management/{kidId}).

### Intermediate Requirements

I finished:

- Administrator can list/create/edit/delete shape for each kid.
- Administrator can login, logout.
- Kid can signup, signin, signout.

I don't finish yet:

- Administrator can create, delete another administrator.

### Challenging Requirements

I finished:

- Administrator can create shape category, they can edit it's name.

I don't finish yet:

- Administrator can manage requirements.
- Administrator can manage area formula.
- Administrator can manage conditions that allows the shape to fall into other categories.

Thank you.
